<%-- 
    Document   : ServiciosAuxiliares
    Created on : 6/07/2022, 12:08:59 a. m.
    Author     : eliof
--%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Servicios Auxiliares - GOBERNACION DE BOLIVAR</title>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="Estilos3.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.rtl.min.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>
    <center><header> 
        <br>
            <center><nav>
                    
                <center id="texto-header-inicio"><b>INVENTARIO- GOBERNACION DE BOLIVAR</b></center>
                
                ROL: DIGITADOR
                    <br> 
                    <br>    
                    <center>
                        <a id="enlaces" href="login.jsp">
                        <button class="boton-navegador-sesion"><b>Cerrar Sesión</b>
                        </button></a>
                        <a id="enlaces" href="ServiciosAuxiliares2.jsp">
                            <button class="boton-navegador-sesion" name="buscar"><b>Buscar</b></button>
                        </a>
                        
                    </center>
                
            </nav></center>
    </header></center>
    
   
<div class="container">   
     <div class="d-flex flex-column flex-shrink-0 p-5 text-white bg-dark" style="width: 340px;" class="item" id="item1">
      <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Activos</font></font></span>
    
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">
      <li>
        <a href="Dependencia.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Dependencias
        </font></font></a>
      </li>
      <li>
        <a href="Persona.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Personas
        </font></font></a>
      </li>
      <a href="Microsoft.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#table"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Microsoft
        </font></font></a>
      </li>
      <li>
        <a href="BDyArchivos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#grid"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          BD y Archivos
        </font></font></a>
      </li>
      <li>
        <a href="SoportesInformaticos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Soportes Informaticos
        </font></font></a>
      </li>
      <li>
        <a href="RedesyComunicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Redes Comunicaciones
        </font></font></a>
      </li>
      <li>
        <a href="Computadores.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Computadores
        </font></font></a>
      </li>
      <li>
        <a href="Aplicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Aplicaciones
        </font></font></a>
      </li>
      <li class="nav-item">
        <a href="#" class="nav-link active" aria-current="page">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servicios Auxiliares
        </font></font></a>
      </li>
      <li>
        <a href="EquipamientoAuxiliar.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Equipamiento Auxiliar
        </font></font></a>
      </li>
      <li>
        <a href="Servidores.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servidores
        </font></font></a>
      </li>
      <li>
        <a href="InstalacionesFisicas.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Instalaciones Fisicas
        </font></font></a>
      </li>
      <li>
        <a href="IPs.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          IPs
        </font></font></a>
      </li>
      <li>
        <a href="MapaRiesgos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Mapa de Riesgos
        </font></font></a>
      </li>
      
    </ul>
    
    <hr>
    <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <a id="enlaces" href="ReporteDependencias.jsp"><span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Reportes</font></font></span></a>
    <hr>
   
    
  </div>
    
        <form class="row g-3" style="width: 840px;">
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Id Servicio</label>
                  <input type="text" name="id_ser" class="form-control" required>
                 </div></center>
                <br>
                                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Nombre</label>
                    <input type="text" name="nom" class="form-control" required>
                </div></center>
                  <br>
                             
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Tipo</label>
                  <select name="tipo" class="form-control">
                    <option value="AD">AD</option>
                    <option value="Internet">Internet</option>
                    <option value="Almacenamiento">Almacenamiento</option>
                    <option value="CorreoElectronico">CorreoElectronico</option>
                    <option value="BD">BD</option>
                    <option value="ERP">ERP</option>
                    <option value="Aplicaciones">Aplicaciones</option>
                    <option value="TelefoniaIP">TelefoniaIP</option>
                    <option value="RedDatos">RedDatos</option>
                    <option value="SoporteTec">SoporteTec</option>
                    <option value="BPM">BPM</option>
                    <option value="Backup">Backup</option>
                    <option value="Monitoreo">Monitoreo</option>
                    <option value="Virtualizacion">Virtualizacion</option>
                    <option value="Publicaciones">Publicaciones</option>
                    <option value="PlataformasGeo">PlataformasGeo</option>
                    <option value="Intranet">Intranet</option>
                    <option value="Moviles">Moviles</option>
                    <option value="GarantiaHardware">GarantiaHardware</option>
                    <option value="SeguridadInformatica">SeguridadInformatica</option>
                    <option value="Servidores">Servidores</option>
                    <option value="RedesInalambricas">RedesInalambricas</option>
                    <option value="VideoConferencia">VideoConferencia</option>
                    <option value="Energia">Energia</option>
                    <option value="Licenciamiento">Licenciamiento</option>
                    <option value="DHCP">DHCP</option>
                    <option value="Nube">Nube</option>
                    <option value="Cifrado">Cifrado</option>
                    <option value="SeguridadAdministrativa">SeguridadAdministrativa</option>
                  </select>
                 </div></center>
                <br>
                  
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Proceso</label>
                    <input type="text" name="proceso" class="form-control" >
                </div></center>
                  <br>
                  
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Descripción</label>
                  <input type="text" name="descripcion" class="form-control" >
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Clasificación</label>
                    <input type="text" name="clasificacion" class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Criticidad</label>
                  <select name="criticidad" class="form-control">
                    <option value="Alta">Alta</option>
                    <option value="Media">Media</option>
                    <option value="Baja">Baja</option>
                  </select>
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Responsable</label>
                    <input type="text" name="responsable" class="form-control" >
                </div></center>
                <br>
                                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Encargado</label>
                    <input type="text" name="encargado" class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Accesos</label>
                    <input type="text" name="accesos" class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Componentes</label>
                    <input type="text" name="componente" class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Contrato Mantenimiento</label>
                    <input type="text" name="cm" class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Fecha Ingreso(AAAA-MM-DD)</label>
                    <input type="text" name="fec_ing" class="form-control"  >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Fecha Salida(AAAA-MM-DD)</label>
                    <input type="text" name="fec_sal" class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Compatibilidad IPv6</label>
                    <input type="text" name="ipv6" class="form-control"  >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Dirección IP</label>
                    <input type="text" name="ip" class="form-control"  >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Instalacion Fisica</label>
                  <select name="instalacion" class="form-control">
                <%
                Connection conexion=null;
                Statement st=null;
                ResultSet resultado=null;
                                         
                try{
                Class.forName("com.mysql.jdbc.Driver");
                conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/inventariogob?user=root&password=130646");
                st=conexion.createStatement();
                resultado=st.executeQuery("select nom_instalaciones_fisicas from instalacionesfisicas");
                while (resultado.next()){
                
                %>
                
                    <option value=<%=resultado.getString(1)%>><%=resultado.getString(1)%></option>
                  
                <%
                }   
                %>
                
                </select>
                 </div></center>
                <br>
                <br>
                <br>
                <br>
                
                <center>
                    <button class="btn btn-primary" type="submit" name="guardar">Guardar</button>
                </center>             
         <%
            if(request.getParameter("guardar")!=null){
                
            long id_ser=Long.parseLong(request.getParameter("id_ser"));
            String nom=request.getParameter("nom");
            String tipo=request.getParameter("tipo");
            String proceso=request.getParameter("proceso");
            String descripcion=request.getParameter("descripcion");
            String clasificacion=request.getParameter("clasificacion");
            String criticidad=request.getParameter("criticidad");            
            String responsable=request.getParameter("responsable");
            String encargado=request.getParameter("encargado");            
            String accesos=request.getParameter("accesos");
            String componente=request.getParameter("componente");
            String cm=request.getParameter("cm");
            String fec_ing=request.getParameter("fec_ing");
            String fec_sal=request.getParameter("fec_sal");
            String ipv6=request.getParameter("ipv6");
            String ip=request.getParameter("ip");
            String instalacion=request.getParameter("instalacion");
            
            ResultSet r4=null;
            r4=st.executeQuery("select id_servicio from serviciosauxiliares where id_servicio="+id_ser+"");
                
            if(r4.next()){
               %>
            <script>
            swal("Error"," ID Existe en ServiciosAuxiliares", "warning");
            </script>
            <% 
            
            }
                ResultSet r5=null;    
                r5=st.executeQuery("select id_instalaciones_fisicas from instalacionesfisicas where nom_instalaciones_fisicas='"+instalacion+"'");
                if(r5.next()){
                    int id_ins=Integer.parseInt(r5.getString(1));
            
                st.executeUpdate("insert into serviciosauxiliares values("+id_ser+",'"+nom+"','"+tipo+"','"+proceso
                       +"','"+descripcion+"','"+clasificacion+"','"+criticidad+"','"+responsable+"','"+encargado
                       +"','"+accesos+"','"+componente+"','"+cm+"','"+fec_ing+"','"+fec_sal+"','"+ipv6
                       +"','"+ip+"',"+id_ins+")");
                %>
            <script>
            swal("Correcto"," Registro Insertado en ServiciosAuxiliares", "success");
            </script>
            <%
                //request.getRequestDispatcher("Dependencia.jsp").forward(request, response);
                conexion.close();
                st.close();
            }
            }                 
            }catch(Exception e){
                System.out.println("Conexión Fallida: "+e);
            
            }
            
            %>
        </form>
    </div>
</div> 
<br>
    <div class="item">
      <center><img class="imagen-logo" src="Imagenes/LOGO GOBERNACION.jpg" alt=""></center>  
    </div> 

    <br>
    <br>
     
</body>
</html>


