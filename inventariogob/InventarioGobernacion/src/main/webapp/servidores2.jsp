<%-- 
    Document   : servidores2
    Created on : 27/07/2022, 10:59:31 p. m.
    Author     : eliof
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Servidor - GOBERNACION DE BOLIVAR</title>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="Estilos3.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.rtl.min.css">
    <link href="/docs/5.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>
    <center><header> 
        <br>
            <center><nav>
                    
                <center id="texto-header-inicio"><b>INVENTARIO- GOBERNACION DE BOLIVAR</b></center>
                
                ROL: DIGITADOR
                    <br> 
                    <br>    
                    <center>
                        <a id="enlaces" href="login.jsp">
                        <button class="boton-navegador-sesion"><b>Cerrar Sesión</b>
                        </button></a>             
                    </center>
            </nav></center>
    </header></center>
    
   
<div class="container">   
     <div class="d-flex flex-column flex-shrink-0 p-5 text-white bg-dark" style="width: 340px;" class="item" id="item1">
      <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Activos</font></font></span>
    
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">
      <li>
        <a href="Dependencia.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Dependencias
        </font></font></a>
      </li>
      <li>
        <a href="Persona.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Personas
        </font></font></a>
      </li>
      <a href="Microsoft.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#table"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Microsoft
        </font></font></a>
      </li>
      <li>
        <a href="BDyArchivos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#grid"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          BD y Archivos
        </font></font></a>
      </li>
      <li>
        <a href="SoportesInformaticos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Soportes Informaticos
        </font></font></a>
      </li>
      <li>
        <a href="RedesyComunicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Redes Comunicaciones
        </font></font></a>
      </li>
      <li>
        <a href="Computadores.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Computadores
        </font></font></a>
      </li>
      <li>
        <a href="Aplicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Aplicaciones
        </font></font></a>
      </li>
      <li>
        <a href="ServiciosAuxiliares.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servicios Auxiliares
        </font></font></a>
      </li>
      <li>
        <a href="EquipamientoAuxiliar.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Equipamiento Auxiliar
        </font></font></a>
      </li>
      <li class="nav-item">
        <a href="Servidores.jsp" class="nav-link active" aria-current="page"">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servidores
        </font></font></a>
      </li>
      <li>
        <a href="InstalacionesFisicas.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Instalaciones Fisicas
        </font></font></a>
      </li>
      <li>
        <a href="IPs.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          IPs
        </font></font></a>
      </li>
      <li>
        <a href="MapaRiesgos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Mapa de Riesgos
        </font></font></a>
      </li>
      
    </ul>
    
    <hr>
    <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <a id="enlaces" href="ReporteDependencias.jsp"><span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Reportes</font></font></span></a>
    <hr>
   
    
  </div>
   
    <div style="width: 840px;">
    <form style="width: 840px;">
        <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Servidor</label>
                  <select name="servidor" class="form-control is-valid">
                <%
                Connection conexion=null;
                Statement st=null;
                ResultSet resultado=null;
                                         
                try{
                Class.forName("com.mysql.jdbc.Driver");
                conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/inventariogob?user=root&password=130646");
                st=conexion.createStatement();
                ResultSet r2=null;
                r2=st.executeQuery("select nom_servidor from servidor");
                while (r2.next()){
                
                %>
                
                    <option value=<%=r2.getString(1)%>><%=r2.getString(1)%></option>
                  
                <%
                }   
                %>
                
                </select>
                 </div></center>
                <br>
                
                <center><button class="btn btn-primary" type="submit" name="buscar">Buscar</button></center>
        </center>
    </form>
    
    <%
            String nom_ser=request.getParameter("servidor");
             
               resultado=st.executeQuery("select *from servidor where nom_servidor='"+nom_ser+"'");
               while(resultado.next()){
        
    %>
                <br>
                <br>
    <form action="" class="row g-3" style="width: 840px;">
    
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Id Servidor</label>
                  <input type="text" name="id_ser" value=<%=resultado.getString(1)%> class="form-control" readonly="readonly">
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Tipo</label>
                  <select name="tipo" value=<%=resultado.getString(2)%> class="form-control">
                    <option value="MisionCritica">MisionCritica</option>
                    <option value="Archivo">Archivo</option>
                    <option value="Aplicaciones">Aplicaciones</option>
                    <option value="Fisico">Fisico</option>
                    <option value="Virtual">Virtual</option>
                    <option value="Linux">Linux</option>
                    <option value="Windows">Windows</option>
                    <option value="ComunicacionMesajeríayVoz">Comunicacion, mesajería y voz</option>
                    <option value="Nas">Nas</option>
                    <option value="BasesdeDatos">Bases de Datos</option>
                    <option value="DHCP">DHCP</option>
                    <option value="AD">AD</option>
                    <option value="DNS">DNS</option>
                    <option value="CorreoElectronico">Correo Electronico</option>
                    <option value="Backup">Backup</option>
                    </select>
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Nombre</label>
                    <input type="text" name="nom" value=<%=resultado.getString(3)%> class="form-control" required>
                </div></center>
                  <br>
                  
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Proceso</label>
                    <input type="text" name="proceso" value=<%=resultado.getString(4)%> class="form-control" >
                </div></center>
                  <br>
                  
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Descripción</label>
                  <input type="text" name="descripcion" value=<%=resultado.getString(5)%> class="form-control" >
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Ubicación</label>
                    <input type="text" name="ubicacion" value=<%=resultado.getString(6)%> class="form-control" >
                </div></center>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Clasificación</label>
                    <input type="text" name="clasificacion" value=<%=resultado.getString(7)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" value=<%=resultado.getString(8)%> class="form-label">Criticidad</label>
                  <select name="criticidad" class="form-control">
                    <option value="Alta">Alta</option>
                    <option value="Media">Media</option>
                    <option value="Baja">Baja</option>
                  </select>
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Responsable</label>
                    <input type="text" name="responsable" value=<%=resultado.getString(9)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Encargado</label>
                    <input type="text" name="encargado" value=<%=resultado.getString(10)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Accesos</label>
                    <input type="text" name="accesos" value=<%=resultado.getString(11)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Contrato Mantenimiento</label>
                    <input type="text" name="cm" value=<%=resultado.getString(12)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Componente</label>
                    <input type="text" name="componente" value=<%=resultado.getString(13)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Fecha Ingreso(AAAA-MM-DD)</label>
                    <input type="text" name="fec_ing" value=<%=resultado.getString(14)%> class="form-control"  >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Fecha Salida(AAAA-MM-DD)</label>
                    <input type="text" name="fec_sal" value=<%=resultado.getString(15)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Compatibilidad IPv6</label>
                    <input type="text" name="ipv6" value=<%=resultado.getString(16)%> class="form-control"  >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Dirección IP</label>
                    <input type="text" name="ip" value=<%=resultado.getString(17)%> class="form-control"  >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Marca</label>
                    <input type="text" name="marca" value=<%=resultado.getString(18)%> class="form-control"  >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Modelo</label>
                    <input type="text" name="modelo" value=<%=resultado.getString(19)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Sistema Operativo</label>
                    <input type="text" name="so" value=<%=resultado.getString(20)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Software Instalado</label>
                    <input type="text" name="si" value=<%=resultado.getString(21)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Rol</label>
                    <input type="text" name="rol" value=<%=resultado.getString(22)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Serie</label>
                    <input type="text" name="serie" value=<%=resultado.getString(23)%> class="form-control">
                </div></center>
                <br>
                <br>
                <br>
                <br>
                
                <center>
                  
                  <button class="btn btn-primary" type="submit" name="actualizar">Actualizar</button>
                  <button class="btn btn-primary" type="submit" name="eliminar">Eliminar</button>
                 
                </center>
                
            <%      
            }
            if(request.getParameter("actualizar")!=null){
                
            long id_ser=Long.parseLong(request.getParameter("id_ser"));
            String tipo=request.getParameter("tipo");
            String nom=request.getParameter("nom");
            String proceso=request.getParameter("proceso");
            String descripcion=request.getParameter("descripcion");
            String ubicacion=request.getParameter("ubicacion");
            String clasificacion=request.getParameter("clasificacion");
            String criticidad=request.getParameter("criticidad");
            String responsable=request.getParameter("responsable");
            String encargado=request.getParameter("encargado");
            String accesos=request.getParameter("accesos");
            String cm=request.getParameter("cm");
            String componente=request.getParameter("componente");
            String fec_ing=request.getParameter("fec_ing");
            String fec_sal=request.getParameter("fec_sal");
            String ipv6=request.getParameter("ipv6");
            String ip=request.getParameter("ip");
            String marca=request.getParameter("marca");
            String modelo=request.getParameter("modelo");
            String so=request.getParameter("so");
            String si=request.getParameter("si");            
            String rol=request.getParameter("rol");
            String serie=request.getParameter("serie");
            
                st.executeUpdate("update servidor set tipo_servidor='"+tipo+"',nom_servidor='"+nom+"',proceso_servidor='"+proceso
                       +"',descripcion_servidor='"+descripcion+"',ubicacion_servidor='"+ubicacion+"',clasificacion_servidor='"+clasificacion
                       +"',criticidad_servidor='"+criticidad+"',responsable_servidor='"+responsable+"',encargado_servidor='"+encargado
                       +"',accesos_servidor='"+accesos+"',contrato_de_mantenimiento_servidor='"+cm+"',componente_servidor='"+componente
                       +"',fec_ing_activo_servidor='"+fec_ing+"',fec_sal_activo_servidor='"+fec_sal+"',compatibilidadipv6_archivo='"+ipv6
                       +"',direccion_ip_servidor='"+ip+"',marca_servidor='"+marca+"',modelo_servidor='"+modelo+"',so_servidor='"+so
                       +"',software_instalado='"+si+"',rol_servidor='"+rol+"',serie_servidor='"+serie+"' where id_servidor="+id_ser+"");
                %>
            <script>
            swal("Correcto"," Registro Actualizado en Servidores", "success");
            </script>
            <%
                //request.getRequestDispatcher("Dependencia.jsp").forward(request, response);
                conexion.close();
                st.close();
            }
            else{
            if(request.getParameter("eliminar")!=null){
                long id_ser=Long.parseLong(request.getParameter("id_ser"));
                st.executeUpdate("delete from servidor where id_servidor="+id_ser+"");
                %>
            <script>
            swal("Correcto"," Registro Eliminado en Servidores", "success");
            </script>
            <%
            }

            }                 
            }catch(Exception e){
                System.out.println("Conexión Fallida: "+e);
            
            }
            
            %>
                
        </form>
    </div>
</div> 
<br>
    <div class="item">
      <center><img class="imagen-logo" src="Imagenes/LOGO GOBERNACION.jpg" alt=""></center>  
    </div> 

    <br>
    <br>
     
</body>
</html>


