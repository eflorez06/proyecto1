<%-- 
    Document   : login
    Created on : 4/08/2022, 12:42:03 p. m.
    Author     : eliof
--%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inicio - INVENTARIO GOBERNACION</title>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="Estilos3.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.rtl.min.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    
    
    </head>
    <body>
        <script>
            function recuperar(){
                swal("Información"," Pongase en contacto con el Administrador del Sistema", "success");
            }
        </script>
        <center><img class="imagen-logo" src="Imagenes/LOGO GOBERNACION.jpg" alt=""></center>
            <center><header> 
                <br>
                
                <br>
                <br>
                    <center><nav>
                            
                        <center id="texto-header-inicio"><b>INVENTARIO - GOBERNACION DE BOLIVAR</b></center>
                    </nav></center>
            </header></center>
            <br>
            <br>
            <form class="row g-3" method="post">
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Usuario</label>
                  <input type="text" name="user" class="form-control is-valid" id="validationServer01" required>
                  <div class="valid-feedback">
                    Digite su Usuario
                  </div>
                </div></center>
                <br>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Contraseña</label>
                    <input type="password" name="pass" class="form-control is-valid" id="validationServer01" required>
                    <div class="valid-feedback">
                      Digite su Contraseña
                    </div>
                  </div></center>
                  <br>
                  <br>

                  <center>
                      <button class="boton-navegador-sesion" type="submit" name="iniciar">Iniciar Sesión</button>
                      <a href="DependenciaG.jsp" id="enlaces2">
                      <button class="boton-navegador-sesion" type="button" name="general">Rol General</button>
                      </a>
                  </center>
                  </form>
                    <br>
                    <center><a href="#" id="enlaces2" onclick="recuperar()"><b>Recuperar Contraseña</b></a></center>
                    <br>
                  
                  <%
                      Connection conexion=null;
                      Statement st=null;
                      
                      try{
                Class.forName("com.mysql.jdbc.Driver");
                conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/inventariogob?user=root&password=130646");
                st=conexion.createStatement();
                if(request.getParameter("iniciar")!=null){
            
                    String user=request.getParameter("user");
                    String pass=request.getParameter("pass");
            
                PreparedStatement pst=conexion.prepareStatement("select usuario,pass from usuario where usuario=? and pass=?");
                //resultado=st.executeQuery("select usuario,pass from usuario where usuario=? and pass=?");
                pst.setString(1,user);
                pst.setString(2,pass);
                ResultSet resultado=pst.executeQuery();
                if(resultado.next()){
                    request.getRequestDispatcher("Dependencia.jsp").forward(request, response);
                }
                else{
                %>
                    <script>
                        swal("Error"," Datos Invalidos", "warning");
                    </script>
                <%
                }
            }                 
            }catch(Exception e){
                out.print("Conexión Fallida: "+e);
            
            }
            
            %>
            
            
    </body>
</html>
