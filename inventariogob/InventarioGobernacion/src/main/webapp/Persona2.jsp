<%-- 
    Document   : Persona2
    Created on : 26/07/2022, 7:01:11 p. m.
    Author     : eliof
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Persona - GOBERNACION DE BOLIVAR</title>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="Estilos3.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.rtl.min.css">
    <link href="/docs/5.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>
    <center><header> 
        <br>
            <center><nav>
                    
                <center id="texto-header-inicio"><b>INVENTARIO- GOBERNACION DE BOLIVAR</b></center>
                
                ROL: DIGITADOR
                    <br> 
                    <br>    
                    <center>
                        <a id="enlaces" href="login.jsp">
                        <button class="boton-navegador-sesion"><b>Cerrar Sesión</b>
                        </button></a>             
                    </center>
            </nav></center>
    </header></center>
    
   
<div class="container">   
     <div class="d-flex flex-column flex-shrink-0 p-5 text-white bg-dark" style="width: 340px;" class="item" id="item1">
      <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Activos</font></font></span>
    
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">
      <li>
        <a href="Dependencia.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Dependencias
        </font></font></a>
      </li>
      <li class="nav-item">
        <a href="Persona.jsp" class="nav-link active" aria-current="page">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Personas
        </font></font></a>
      </li>
      <a href="Microsoft.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#table"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Microsoft
        </font></font></a>
      </li>
      <li>
        <a href="BDyArchivos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#grid"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          BD y Archivos
        </font></font></a>
      </li>
      <li>
        <a href="SoportesInformaticos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Soportes Informaticos
        </font></font></a>
      </li>
      <li>
        <a href="RedesyComunicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Redes Comunicaciones
        </font></font></a>
      </li>
      <li>
        <a href="Computadores.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Computadores
        </font></font></a>
      </li>
      <li>
        <a href="Aplicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Aplicaciones
        </font></font></a>
      </li>
      <li>
        <a href="ServiciosAuxiliares.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servicios Auxiliares
        </font></font></a>
      </li>
      <li>
        <a href="EquipamientoAuxiliar.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Equipamiento Auxiliar
        </font></font></a>
      </li>
      <li>
        <a href="Servidores.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servidores
        </font></font></a>
      </li>
      <li>
        <a href="InstalacionesFisicas.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Instalaciones Fisicas
        </font></font></a>
      </li>
      <li>
        <a href="IPs.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          IPs
        </font></font></a>
      </li>
      <li>
        <a href="MapaRiesgos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Mapa de Riesgos
        </font></font></a>
      </li>
      
    </ul>
    
    <hr>
    <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <a id="enlaces" href="ReporteDependencias.jsp"><span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Reportes</font></font></span></a>
    <hr>
   
    
  </div>
    
    <div style="width: 840px;">
    <form style="width: 840px;">
        <center><div class="col-md-4">
                <label for="validationServer01" class="form-label">Id Persona</label>
                  <input type="text" name="id" class="form-control is-valid" id="validationServer01" >
        </div></center>
                <br>
                
                <center><button class="btn btn-primary" type="submit" name="buscar">Buscar</button></center>
        </center>
    </form>
    
    <%
            String id=request.getParameter("id");
            
            
            Connection conexion=null;
            Statement st=null;
            ResultSet resultado=null;
            
            try{
            Class.forName("com.mysql.jdbc.Driver");
            conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/inventariogob?user=root&password=130646");
            st=conexion.createStatement();
            
            
               resultado=st.executeQuery("select *from persona where id_persona="+id+"");
               while(resultado.next()){
        
    %>
                <br>
                <br>
    <form style="width: 840px;">
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Id Persona</label>
                  <input type="text" name="id_per" value=<%=resultado.getString(1)%> class="form-control" readonly="readonly">
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Nombre</label>
                    <input type="text" name="nom_per" value=<%=resultado.getString(2)%> class="form-control" required>
                </div></center>
                  <br>
                  
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Tipo</label>
                  <input type="text" name="tipo_per" value=<%=resultado.getString(3)%> class="form-control">
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Rol</label>
                    <input type="text" name="rol_per" value=<%=resultado.getString(4)%> class="form-control">
                </div></center>
                  <br>
                  
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Proceso</label>
                  <input type="text" name="proceso_per" value=<%=resultado.getString(5)%> class="form-control">
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Clasificación</label>
                    <input type="text" name="clasificacion_per" value=<%=resultado.getString(6)%> class="form-control">
                </div></center>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Justificación</label>
                    <input type="text" name="justificacion_per" value=<%=resultado.getString(7)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Propietario</label>
                    <input type="text" name="propietario_per" value=<%=resultado.getString(8)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Accesos</label>
                    <input type="text" name="accesos_per" value=<%=resultado.getString(9)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Fecha Ingreso(AAAA-MM-DD)</label>
                    <input type="text" name="fec_ing_per" value=<%=resultado.getString(10)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Fecha Salida(AAAA-MM-DD)</label>
                    <input type="text" name="fec_sal_per" value=<%=resultado.getString(11)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Dependencia</label>
                  <input type="text" name="id_dep" value=<%=resultado.getString(12)%> class="form-control">
                 </div></center>
                <br>
                <br>
                <br>
                <br>
                
                <center>
                  
                    <button class="btn btn-primary" type="submit" name="actualizar">Actualizar</button>
                    <button class="btn btn-primary" type="submit" name="eliminar">Eliminar</button>
                </center>
                
               <%      
            }
        if(request.getParameter("actualizar")!=null){

            long idd=Long.parseLong(request.getParameter("id_per"));
            String nom=request.getParameter("nom_per");
            String tipo=request.getParameter("tipo_per");
            String rol=request.getParameter("rol_per");
            String proceso=request.getParameter("proceso_per");
            String clasificacion=request.getParameter("clasificacion_per");
            String justificacion=request.getParameter("justificacion_per");
            String propietario=request.getParameter("propietario_per");
            String accesos=request.getParameter("accesos_per");
            String fec_ing=request.getParameter("fec_ing_per");
            String fec_sal=request.getParameter("fec_sal_per");
            String id_dep=request.getParameter("id_dep");

            st.executeUpdate("update persona set nom_persona='"+nom
                                +"',tipo_persona='"+tipo+"',rol_persona='"+rol+"',proceso_persona='"+proceso
                                +"',clasificacion_persona='"+clasificacion+"',justificacion_persona='"+justificacion
                                +"',propietario_informacion_a_cargo='"+propietario+"',accesos_persona='"+accesos+"',fec_ing_activo_persona='"+fec_ing
                                +"',fe_sal_activo_persona='"+fec_sal+"',Dependencia_id_dependencia='"+id_dep+"' where id_persona="+idd+"" );
             %>
            <script>
            swal("Correcto"," Registro Actualizado en Persona", "success");
            </script>
            <%
        }
        else{
            if(request.getParameter("eliminar")!=null){
                long idd=Long.parseLong(request.getParameter("id_per"));
                st.executeUpdate("delete from persona where id_persona="+idd+"");
            %>
            <script>
            swal("Correcto"," Registro Eliminado en Persona", "success");
            </script>
            <%
            }

        }
        
        }catch(Exception e){
                System.out.println("Conexión Fallida: "+e);}
        
          
  
            %>
                
        </form>
    </div>
</div> 
<br>
    <div class="item">
      <center><img class="imagen-logo" src="Imagenes/LOGO GOBERNACION.jpg" alt=""></center>  
    </div> 

    <br>
    <br>
     
</body>
</html>

