<%-- 
    Document   : Microsoft2
    Created on : 30/07/2022, 4:21:57 p. m.
    Author     : eliof
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Microsoft - GOBERNACION DE BOLIVAR</title>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="Estilos3.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.rtl.min.css">
    <link href="/docs/5.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>
    <center><header> 
        <br>
            <center><nav>
                    
                <center id="texto-header-inicio"><b>INVENTARIO- GOBERNACION DE BOLIVAR</b></center>
                
                ROL: DIGITADOR
                    <br> 
                    <br>    
                    <center>
                        <a id="enlaces" href="login.jsp">
                        <button class="boton-navegador-sesion"><b>Cerrar Sesión</b>
                        </button></a>             
                    </center>
            </nav></center>
    </header></center>
    
   
<div class="container">   
     <div class="d-flex flex-column flex-shrink-0 p-5 text-white bg-dark" style="width: 340px;" class="item" id="item1">
      <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Activos</font></font></span>
    
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">
      <li>
        <a href="Dependencia.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Dependencias
        </font></font></a>
      </li>
      <li>
        <a href="Persona.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Personas
        </font></font></a>
      </li>
      <li class="nav-item">
        <a href="Microsoft.jsp" class="nav-link active" aria-current="page">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#table"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Microsoft
        </font></font></a>
      </li>
      <li>
        <a href="BDyArchivos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#grid"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          BD y Archivos
        </font></font></a>
      </li>
      <li>
        <a href="SoportesInformaticos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Soportes Informaticos
        </font></font></a>
      </li>
      <li>
        <a href="RedesyComunicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Redes Comunicaciones
        </font></font></a>
      </li>
      <li>
        <a href="Computadores.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Computadores
        </font></font></a>
      </li>
      <li>
        <a href="Aplicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Aplicaciones
        </font></font></a>
      </li>
      <li>
        <a href="ServiciosAuxiliares.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servicios Auxiliares
        </font></font></a>
      </li>
      <li>
        <a href="EquipamientoAuxiliar.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Equipamiento Auxiliar
        </font></font></a>
      </li>
      <li>
        <a href="Servidores.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servidores
        </font></font></a>
      </li>
      <li>
        <a href="InstalacionesFisicas.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Instalaciones Fisicas
        </font></font></a>
      </li>
      <li>
        <a href="IPs.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          IPs
        </font></font></a>
      </li>
      <li>
        <a href="MapaRiesgos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Mapa de Riesgos
        </font></font></a>
      </li>
      
    </ul>
    
    <hr>
    <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <a id="enlaces" href="ReporteDependencias.jsp"><span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Reportes</font></font></span></a>
    <hr>
   
    
  </div>
    <div>
        <form style="width: 840px;">
        <center><div class="col-md-4">
                <label for="validationServer01" class="form-label">Id Microsoft</label>
                  <input type="text" name="id" class="form-control is-valid" id="validationServer01" >
        </div></center>
                <br>
                
                <center><button class="btn btn-primary" type="submit" name="buscar">Buscar</button></center>
        </center>
    </form>
    
    <%
            String id=request.getParameter("id");
            
            
            Connection conexion=null;
            Statement st=null;
            ResultSet resultado=null;
            
            try{
            Class.forName("com.mysql.jdbc.Driver");
            conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/inventariogob?user=root&password=130646");
            st=conexion.createStatement();
            
            
               resultado=st.executeQuery("select *from microsoft where id_microsoft="+id+"");
               while(resultado.next()){
        
    %>
                <br>
                <br>
    <form style="width: 840px;">
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Id Microsoft</label>
                  <input type="text" name="id_mic" value=<%=resultado.getString(1)%> class="form-control" readonly="readonly">
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Grupo</label>
                    <select name="grupo" value=<%=resultado.getString(2)%> class="form-control">
                    <option value="Aplication">Aplication</option>
                    <option value="Server">Server</option>
                    <option value="System">System</option>
                  </select>
                </div></center>
                <br>
                  
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Familia</label>
                  <select name="familia" value=<%=resultado.getString(3)%> class="form-control">
                    <option value="OfficeProfesional">OfficeProfesional</option>
                    <option value="OfficeStandard">OfficeStandard</option>
                    <option value="SQL-UserCAL">SQL-UserCAL</option>
                    <option value="SQLServer-Standard">SQLServer-Standard</option>
                    <option value="SQLServer-StandardCore">SQLServer-StandardCore</option>
                    <option value="WindowsServer-Standard">WindowsServer-Standard</option>
                    <option value="WindowsServer-UserCAL">WindowsServer-UserCAL</option>
                    <option value="WindowsServerCAL">WindowsServerCAL</option>
                    <option value="Windows">Windows</option>
                    <option value="WindowsConsumer">WindowsConsumer</option>
                  </select>
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Versión</label>
                    <input type="text" name="version" value=<%=resultado.getString(4)%> class="form-control">
                </div></center>
                  <br>
                  
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">cantidad Real</label>
                  <input type="text" name="real" value=<%=resultado.getString(5)%> class="form-control">
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Cantidad Asignada</label>
                    <input type="text" name="asignadas" value=<%=resultado.getString(6)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Id Computador</label>
                    <input type="text" name="id_pc" value=<%=resultado.getString(7)%> class="form-control">
                </div></center>
                <br>
                <br>
                <br>
                <br>
                <br>
                
                <center>
                    <button class="btn btn-primary" type="submit" name="actualizar">Actualizar</button>
                    <button class="btn btn-primary" type="submit" name="eliminar">Eliminar</button>
                </center>
                
            <%
            }
            if(request.getParameter("actualizar")!=null){
                
            long id_mic=Long.parseLong(request.getParameter("id_mic"));
            String grupo=request.getParameter("grupo");
            String familia=request.getParameter("familia");
            float version=Float.parseFloat(request.getParameter("version"));
            int real=Integer.parseInt(request.getParameter("real"));
            int asignadas=Integer.parseInt(request.getParameter("asignadas"));
            long id_pc=Long.parseLong(request.getParameter("id_pc"));

                st.executeUpdate("update microsoft set grupo='"+grupo+"',familia='"+familia
                                  +"',version="+version+",cantidad_real="+real+",cantidad_asignada="+asignadas+",Computador_id_computador="+id_pc+" where id_microsoft="+id_mic+"");
                %>
                <script>
                swal("Correcto"," Registro Actualizado en Microsoft", "success");
                </script>
                <%
                conexion.close();
                st.close();
            }
            else{
            if(request.getParameter("eliminar")!=null){
                long id_mic=Long.parseLong(request.getParameter("id_mic"));
                st.executeUpdate("delete from microsoft where id_microsoft="+id_mic+"");
                %>
                <script>
                swal("Correcto"," Registro Eliminado en Microsoft", "success");
                </script>
                <%
            }

            }                  
            }catch(Exception e){
                System.out.println("Conexión Fallida: "+e);
            
            }
            
            %>
                
        </form>
    </div>
</div> 
<br>
    <div class="item">
      <center><img class="imagen-logo" src="Imagenes/LOGO GOBERNACION.jpg" alt=""></center>  
    </div> 

    <br>
    <br>
     
</body>
</html>
