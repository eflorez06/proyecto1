<%-- 
    Document   : MapaRiesgos
    Created on : 22/08/2022, 5:55:07 p. m.
    Author     : eliof
--%>



<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mapa de Riesgos - GOBERNACION DE BOLIVAR</title>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="Estilos3.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.rtl.min.css">
</head>
<body>
    <center><header> 
        <br>
            <center><nav>   
                <center id="texto-header-inicio"><b>MAPA DE RIESGOS DE SEGURIDAD DE LA INFORMACIÓN</b></center>
                
                
                    ROL: DIGITADOR
                    <br> 
                    <br>    
                    <center>
                        <a id="enlaces" href="login.jsp">
                        <button class="boton-navegador-sesion">
                            <b>Cerrar Sesión</b>
                        </button></a>
                    </center>
                
            </nav></center>
    </header></center>
    
   
<div class="container">   
     <div class="d-flex flex-column flex-shrink-0 p-5 text-white bg-dark" style="width: 340px;" class="item" id="item1">
      <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Activos</font></font></span>
    
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">
      <li class="nav-item">
        <a href="Dependencia.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Dependencias
        </font></font></a>
      </li>
      <li>
        <a href="Persona.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Personas
        </font></font></a>
      </li>
      <li>
        <a href="Microsoft.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#table"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Microsoft
        </font></font></a>
      </li>
      <li>
        <a href="BDyArchivos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#grid"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          BD y Archivos
        </font></font></a>
      </li>
      <li>
        <a href="SoportesInformaticos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Soportes Informaticos
        </font></font></a>
      </li>
      <li>
        <a href="RedesyComunicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Redes Comunicaciones
        </font></font></a>
      </li>
      <li>
        <a href="Computadores.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Computadores
        </font></font></a>
      </li>
      <li>
        <a href="Aplicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Aplicaciones
        </font></font></a>
      </li>
      <li>
        <a href="ServiciosAuxiliares.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servicios Auxiliares
        </font></font></a>
      </li>
      <li>
        <a href="EquipamientoAuxiliar.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Equipamiento Auxiliar
        </font></font></a>
      </li>
      <li>
        <a href="Servidores.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servidores
        </font></font></a>
      </li>
      <li>
        <a href="InstalacionesFisicas.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Instalaciones Fisicas
        </font></font></a>
      </li>
      <li>
        <a href="IPs.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          IPs
        </font></font></a>
      </li>
      <li>
        <a href="#" class="nav-link active" aria-current="page">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Mapa de Riesgos
        </font></font></a>
      </li>
      
    </ul>
    
    <hr>
    <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <a id="enlaces" href="ReporteDependencias.jsp"><span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Reportes</font></font></span></a>
    <hr>
    
  </div>
   

<%! 
 public ResultSet dataTipo(Connection con, String sql){
    Statement st=null;
    ResultSet res=null;
    try {
           st=con.createStatement(); 
           res = st.executeQuery(sql);
           
        } catch (Exception e) {
    }
    
   return res;
  }
%>
    
    <%    
          
        
            Connection conexion=null;
            Statement st=null;
            ResultSet resultado=null;
        
            try{
                Class.forName("com.mysql.jdbc.Driver");
                conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/inventariogob?user=root&password=130646");
                
                st=conexion.createStatement();
                ResultSet res1=null;
            %>


    
    <script>
                
        function mostrar_activo(tipo){
       
        const selectActivo = document.getElementById("activo");
        if(tipo=="Persona"){
            selectActivo.innerHTML = "";
            selectActivo.innerHTML = "<option value='0'>Seleccione...</option>";
                            <%
                            res1 = dataTipo(conexion, "select nom_persona from persona order by nom_persona ASC");
                            while (res1.next()){

                            %>

                              selectActivo.innerHTML += "<option value='<%=res1.getString(1)%>'><%=res1.getString(1)%></option>"

                            <%
                            }   
                            %>
            
        }
        else if(tipo=="0"){
            
            selectActivo.innerHTML = "";
        }
        else if(tipo=="Computador"){
            selectActivo.innerHTML = "";
            selectActivo.innerHTML = "<option value='0'>Seleccione...</option>";
          <%
                            res1 = dataTipo(conexion, "select nom_computador from computador order by nom_computador ASC");
                            while (res1.next()){

                            %>

                              selectActivo.innerHTML += "<option value='<%=res1.getString(1)%>'><%=res1.getString(1)%></option>"

                            <%
                            }   
                            %>
  
        }else if(tipo=="Servidor"){
            selectActivo.innerHTML = "";
            selectActivo.innerHTML = "<option value='0'>Seleccione...</option>";
          <%
                            res1 = dataTipo(conexion, "select nom_servidor from servidor order by nom_servidor ASC");
                            //res1=st.executeQuery("select nom_persona from persona order by nom_persona ASC");
                            while (res1.next()){

                            %>

                              selectActivo.innerHTML += "<option value='<%=res1.getString(1)%>'><%=res1.getString(1)%></option>"

                            <%
                            }   
                            %>
  
        }else if(tipo=="SoporteInformatico"){
            selectActivo.innerHTML = "";
            selectActivo.innerHTML = "<option value='0'>Seleccione...</option>";
          <%
                            res1 = dataTipo(conexion, "select nom_soporte_informatico from soportesinformaticos order by nom_soporte_informatico ASC");
                            while (res1.next()){

                            %>

                              selectActivo.innerHTML += "<option value='<%=res1.getString(1)%>'><%=res1.getString(1)%></option>"

                            <%
                            }   
                            %>
  
        }else if(tipo=="RedesyComunicaciones"){
            selectActivo.innerHTML = "";
            selectActivo.innerHTML = "<option value='0'>Seleccione...</option>";
          <%
                            res1 = dataTipo(conexion, "select nom_redes_comunicaciones from redesycomunicaciones order by nom_redes_comunicaciones ASC");
                            while (res1.next()){

                            %>

                              selectActivo.innerHTML += "<option value='<%=res1.getString(1)%>'><%=res1.getString(1)%></option>"

                            <%
                            }   
                            %>
  
        }else if(tipo=="Aplicacion"){
            selectActivo.innerHTML = "";
            selectActivo.innerHTML = "<option value='0'>Seleccione...</option>";
          <%
                            res1 = dataTipo(conexion, "select nom_aplicacion from aplicacion order by nom_aplicacion ASC");
                            while (res1.next()){

                            %>

                              selectActivo.innerHTML += "<option value='<%=res1.getString(1)%>'><%=res1.getString(1)%></option>"

                            <%
                            }   
                            %>
  
        }else if(tipo=="Microsoft"){
            selectActivo.innerHTML = "";
            selectActivo.innerHTML = "<option value='0'>Seleccione...</option>";
          <%
                            res1 = dataTipo(conexion, "select id_microsoft from microsoft order by id_microsoft ASC");
                            //res1=st.executeQuery("select nom_persona from persona order by nom_persona ASC");
                            while (res1.next()){

                            %>

                              selectActivo.innerHTML += "<option value='<%=res1.getString(1)%>'><%=res1.getString(1)%></option>"

                            <%
                            }   
                            %>
  
        }else if(tipo=="BDyArchivos"){
            selectActivo.innerHTML = "";
            selectActivo.innerHTML = "<option value='0'>Seleccione...</option>";
          <%
                            res1 = dataTipo(conexion, "select nom_archivo from bdyarchivo order by nom_archivo ASC");
                            while (res1.next()){

                            %>

                              selectActivo.innerHTML += "<option value='<%=res1.getString(1)%>'><%=res1.getString(1)%></option>"

                            <%
                            }   
                            %>
  
        }else if(tipo=="ServiciosAuxiliares"){
            selectActivo.innerHTML = "";
            selectActivo.innerHTML = "<option value='0'>Seleccione...</option>";
          <%
                            res1 = dataTipo(conexion, "select nom_servicio from serviciosauxiliares order by nom_servicio ASC");
                            while (res1.next()){

                            %>

                              selectActivo.innerHTML += "<option value='<%=res1.getString(1)%>'><%=res1.getString(1)%></option>"

                            <%
                            }   
                            %>
  
        }else if(tipo=="EquipamientoAuxiliar"){
            selectActivo.innerHTML = "";
            selectActivo.innerHTML = "<option value='0'>Seleccione...</option>";
          <%
                            res1 = dataTipo(conexion, "select nom_equipamiento_auxiliar from equipamientoauxiliar order by nom_equipamiento_auxiliar ASC");
                            while (res1.next()){

                            %>

                              selectActivo.innerHTML += "<option value='<%=res1.getString(1)%>'><%=res1.getString(1)%></option>"

                            <%
                            }   
                            %>
  
        }else if(tipo=="InstalacionesFisicas"){
            selectActivo.innerHTML = "";
            selectActivo.innerHTML = "<option value='0'>Seleccione...</option>";
          <%
                            res1 = dataTipo(conexion, "select nom_instalaciones_fisicas from instalacionesfisicas order by nom_instalaciones_fisicas ASC");
                            while (res1.next()){

                            %>

                              selectActivo.innerHTML += "<option value='<%=res1.getString(1)%>'><%=res1.getString(1)%></option>"

                            <%
                            }   
                            %>
  
        }else if(tipo=="IPs"){
            selectActivo.innerHTML = "";
            selectActivo.innerHTML = "<option value='0'>Seleccione...</option>";
          <%
                            res1 = dataTipo(conexion, "select direccion_ip_real from ips order by direccion_ip_real ASC");
                            while (res1.next()){

                            %>

                              selectActivo.innerHTML += "<option value='<%=res1.getString(1)%>'><%=res1.getString(1)%></option>"

                            <%
                            }   
                            %>
  
        }
                
        }
        
    </script>
     
    <script>
        
        var Informacion=["Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información",
        "Manejo manual de la información","Ausencia de validación de autenticación de la información","Ausencia de copias de respaldo o backups de la información",
        "Retraso en la salida de información de los sistemas","Retraso en la entrega de información por parte del personal",
        "Información sensible sin cifrado","Ausencia o deficiencia en los sistemas de autenticación de los aplicativos",
        "Deficiencia en la autorización de permisos de la información"];
        var Software=["Ausencia o insuficiencia de pruebas de software","Defectos bien conocidos en el software",
        "Ausencia de terminación de la sesión cuando se abandona la estación de trabajo","Disposición o reutilización de los medios de almacenamiento sin borrado adecuado",
        "Ausencia de pistas de audotoría","Asignación errada de los derechos de acceso",
        "Software ampliamente distribuido","En términos de tiempo utilización de datos errados en los programas de aplicación",
        "Interfaz de usuario compleja","Ausencia de documentación","Configuración incorrecta de parámetros",
        "Fechas incorrectas","Ausencia de mecanismo de identificación y autenticación, como la autenticación de usuario","Tablas de contraseñas sin protección",
        "Gestión deficiente de las contraseñas","Habilitación de servicios innecesarios","Software nuevo o inmaduro",
        "Especificaciones incompletas o no claras para los desarrolladores","Ausencia de control de cambios eficaz",
        "Descarga y uso no controlados de software","Ausencia de copias de respaldo","Ausencia de protección física de la edificación, puertas y ventanas",
        "Falla en la producción de informes de gestión"];
        var Hardware=["Mantenimiento insuficiente/instalación fallida de los medios de almacenamiento","Ausencia de esquemas de reemplazo periódico",
        "Susceptibilidad a la humedad, el polvo y la suciedad","Sensibilidad a la radiación electromagnética",
        "Ausencia de un eficiente control de cambios en la configuración","Susceptibilidad a la variaciones de voltaje",
        "Susceptibilidad a las variaciones de temperatura","Almacenamiento sin protección",
        "Falta de cuidado en la disposición final","Copia no controlada","Ausencia de pruebas de envío o recepción de mensajes",
        "Líneas de comunicación sin protección","Tráfico sensible sin protección","Conexión deficiente de los cables",
        "Punto único de falla","Ausencia de identificación y autenticación de emisor y receptor",
        "Arquitectura insegura de la red","Transferencia de contraseñas en claro",
        "Gestión inadecuada de la red (Tolrancia a fallas en el enrutamiento)","Conexiones de red pública sin protección"];
        var InfraestructuraFisica=["Uso inadecuado o descuidado del control de acceso físico a las edificaciones y los recintos",
        "Ubicación en un área susceptible de inundación","Red energética Inestable","Ausencia de protección física de la edificación, puertas y ventanas"];
        var Organizacionales=["Ausencia de procedimeinto formal para el registro y retiro de usuarios","Ausencia de proceso formal para la revisión (supervisión) de los derechos de acceso",
        "Ausencia o insuficiencia de disposiciones (con respecto a la seguridad) en los contratos con los clientes y/o terceras partes","Ausencia de procedimiento de monitoreo de los recursos de procesamiento de información",
        "Ausencia de auditorías (supervisiones) regulares","Ausencia de procedimientos de identificación y valoración de riesgos",
        "Ausencia de reportes de fallas en los registros de administradores y operadores","Respuesta inadecuada de mantenimiento de servicio",
        "Ausencia de acuerdos de nivel de servicio, o insuficiencia en los mismos","Ausencia de procedimiento de control de cambios",
        "Ausencia de procedimiento formal para el control de la documentación del SGSI","Ausencia de procedimiento formal para la supervisión del registro del SGSI",
        "Ausencia de procedimiento formal para la autorización de la información disponible al público","Ausencia de asignación adecuada de responsabilidades en la seguridad de la información",
        "Ausencia de planes de continuidad","Ausencia de políticas sobre el uso del correo electrónico","Ausencia de procedimientos para la introducción del software en los sistemas operativos",
        "Ausencia de registros en las bitácoras (logs) de administrador operario","Ausencia de procedimientos para el manejo de información clasificada",
        "Ausencia de responsabilidades en la seguridad de la información en la descripción de los cargos","Ausencia de procesos disciplinarios definidos en el caso de incidentes de seguridad de la información",
        "Ausencia de política formal sobre la utilización de computadores portátiles","Ausencia de control de los activos que se encuentra fuera de las instalaciones",
        "Ausencia o insuficiencia de política sobre limpieza de escritorio y de pantalla","Ausencia de autorización de los recursos de procesamiento de la información",
        "Ausencia de mecanismos de monitoreo establecidos para las brechas en la seguridad","Ausencia de revisiones regulares por parte de la gerencia",
        "Ausencia de procedimientos para la presentación de informes sobre las debilidades en la seguridad","Ausencia de procedimientos del cumplimiento de las disposiciones con los derechos intelectuales"];
        var RecursosHumanos=["Ausencia del personal","Procedimientos inadecuados de contratación","Entrenamiento insuficiente en seguridad",
        "Uso incorrecto de software y hardware","Falla de conciencia acerca de la seguridad","Ausencia de mecanismos de monitoreo",
        "Trabajo o supervisado del personal externo o de limpieza","Ausencia de políticas para el uso correcto de los medios de telecomunicaciones y mensajería"];
    
        function calcular_vulnerabilidad(event){
        var tipo;
        tipo = event.target.value;
        if(tipo=="Persona"){
            mostrar_activo(tipo);
            activo=eval(RecursosHumanos);
            var tam = activo.length;
            document.generar.vulnerabilidad.length = tam;
            for(var i=0;i<tam;i++){ 
                document.generar.vulnerabilidad.options[i].value=activo[i]; 
         	document.generar.vulnerabilidad.options[i].text=activo[i];
            }
        }else if(tipo=="Computador"){
            mostrar_activo(tipo);
            activo=eval(Hardware);
            var tam = activo.length;
            document.generar.vulnerabilidad.length = tam;
            for(var i=0;i<tam;i++){ 
                document.generar.vulnerabilidad.options[i].value=activo[i]; 
         	document.generar.vulnerabilidad.options[i].text=activo[i];
            }
        }else if(tipo=="Servidor"){
            mostrar_activo(tipo);
            activo=eval(Hardware);
            var tam = activo.length;
            document.generar.vulnerabilidad.length = tam;
            for(var i=0;i<tam;i++){ 
                document.generar.vulnerabilidad.options[i].value=activo[i]; 
         	document.generar.vulnerabilidad.options[i].text=activo[i];
            }
        }else if(tipo=="SoporteInformatico"){
            mostrar_activo(tipo);
            activo=eval(Hardware);
            var tam = activo.length;
            document.generar.vulnerabilidad.length = tam;
            for(var i=0;i<tam;i++){ 
                document.generar.vulnerabilidad.options[i].value=activo[i]; 
         	document.generar.vulnerabilidad.options[i].text=activo[i];
            }
        }else if(tipo=="RedesyComunicaciones"){
            mostrar_activo(tipo);
            activo=eval(Hardware);
            var tam = activo.length;
            document.generar.vulnerabilidad.length = tam;
            for(var i=0;i<tam;i++){ 
                document.generar.vulnerabilidad.options[i].value=activo[i]; 
         	document.generar.vulnerabilidad.options[i].text=activo[i];
            }
        }else if(tipo=="Aplicacion"){
            mostrar_activo(tipo);
            activo=eval(Software);
            var tam = activo.length;
            document.generar.vulnerabilidad.length = tam;
            for(var i=0;i<tam;i++){ 
                document.generar.vulnerabilidad.options[i].value=activo[i]; 
         	document.generar.vulnerabilidad.options[i].text=activo[i];
            }
        }else if(tipo=="Microsoft"){
            mostrar_activo(tipo);
            activo=eval(Software);
            var tam = activo.length;
            document.generar.vulnerabilidad.length = tam;
            for(var i=0;i<tam;i++){ 
                document.generar.vulnerabilidad.options[i].value=activo[i]; 
         	document.generar.vulnerabilidad.options[i].text=activo[i];
            }
        }else if(tipo=="BDyArchivos"){
            mostrar_activo(tipo);
            activo=eval(Informacion);
            var tam = activo.length;
            document.generar.vulnerabilidad.length = tam;
            for(var i=0;i<tam;i++){ 
                document.generar.vulnerabilidad.options[i].value=activo[i]; 
         	document.generar.vulnerabilidad.options[i].text=activo[i];
            }
        }else if(tipo=="InstalacionesFisicas"){
            mostrar_activo(tipo);
            activo=eval(InfraestructuraFisica);
            var tam = activo.length;
            document.generar.vulnerabilidad.length = tam;
            for(var i=0;i<tam;i++){ 
                document.generar.vulnerabilidad.options[i].value=activo[i]; 
         	document.generar.vulnerabilidad.options[i].text=activo[i];
            }
        }else if(tipo=="IPs" ){
            mostrar_activo(tipo);
            activo=eval(Organizacionales);
            var tam = activo.length;
            document.generar.vulnerabilidad.length = tam;
            for(var i=0;i<tam;i++){ 
                document.generar.vulnerabilidad.options[i].value=activo[i]; 
         	document.generar.vulnerabilidad.options[i].text=activo[i];
            }
        }else if(tipo=="ServiciosAuxiliares"){
            mostrar_activo(tipo);
            activo=eval(Organizacionales);
            var tam = activo.length;
            document.generar.vulnerabilidad.length = tam;
            for(var i=0;i<tam;i++){ 
                document.generar.vulnerabilidad.options[i].value=activo[i]; 
         	document.generar.vulnerabilidad.options[i].text=activo[i];
            }
        }else if(tipo=="EquipamientoAuxiliar"){
            mostrar_activo(tipo);
            activo=eval(Organizacionales);
            var tam = activo.length;
            document.generar.vulnerabilidad.length = tam;
            for(var i=0;i<tam;i++){ 
                document.generar.vulnerabilidad.options[i].value=activo[i]; 
         	document.generar.vulnerabilidad.options[i].text=activo[i];
            }
        }else{ 
            document.generar.vulnerabilidad.length = 1;
            document.generar.vulnerabilidad.options[0].value = " "; 
            document.generar.vulnerabilidad.options[0].text = "SIN ASIGNAR"; 
            }
        }
    </script>
    
    <div class="cuadrado2">
        
        <form id="generar" name="generar" method="post">
        <table class="table table-bordered" border="1" text align="center">
            
            <tr bgcolor="skyblue">
                <td class="u-border-1 u-border-grey-25 u-table-cell" colspan="2"><center><b>ACTIVOS</b></center></td>
                <td class="u-border-1 u-border-grey-25 u-table-cell" colspan="6"><center><b>IDENTIFICACIÓN DEL RIESGO</b></center></td>
                <td class="u-border-1 u-border-grey-25 u-table-cell" colspan="3"><center><b>ANÁLISIS DEL RIESGO INHERENTE</b></center></td>
                <td class="u-border-1 u-border-grey-25 u-table-cell" colspan="3"><center><b>IDENTIFICACIÓN DE CONTROLES</b></center></td>
                <td class="u-border-1 u-border-grey-25 u-table-cell" colspan="3"><center><b>RIESGO RESIDUAL</b></center></td>
                <td class="u-border-1 u-border-grey-25 u-table-cell" colspan="8"><center><b>MANEJO DEL RIESGO RESIDUAL - Plan de Tratamiento de Riesgos</b></center></td>
            </tr>
            <tr bgcolor="skyblue">
                <td class="u-border-1 u-border-grey-25 u-table-cell">Tipo_de_Activo_de_Información </td><td class="u-border-1 u-border-grey-25 u-table-cell">Activo_de_Información </td>
                <td class="u-border-1 u-border-grey-25 u-table-cell">Numero </td><td class="u-border-1 u-border-grey-25 u-table-cell">Propiedad_que_afecta_el_Riesgo </td>
                <td class="u-border-1 u-border-grey-25 u-table-cell">Descripción_del_Riesgo </td><td class="u-border-1 u-border-grey-25 u-table-cell">Responsable_de_determinar_la_materialización_del_riesgo </td>
                <td class="u-border-1 u-border-grey-25 u-table-cell">Amenazas </td><td class="u-border-1 u-border-grey-25 u-table-cell">Vulnerabilidades </td>
                <td class="u-border-1 u-border-grey-25 u-table-cell">Probabilidad </td><td class="u-border-1 u-border-grey-25 u-table-cell">Impacto </td>
                <td class="u-border-1 u-border-grey-25 u-table-cell">Zona_de_Riesgo </td><td class="u-border-1 u-border-grey-25 u-table-cell">Opciones_de_manejo_del_Riesgo </td>
                <td class="u-border-1 u-border-grey-25 u-table-cell">Descripción_del_control </td><td class="u-border-1 u-border-grey-25 u-table-cell">Responsable_de_ejecutar_el_Control </td>
                <td class="u-border-1 u-border-grey-25 u-table-cell">Probabilidad </td><td class="u-border-1 u-border-grey-25 u-table-cell">Impacto </td>
                <td class="u-border-1 u-border-grey-25 u-table-cell">Zona_de_Riesgo_Residual </td><td class="u-border-1 u-border-grey-25 u-table-cell">Opciones_de_manejo_del_Riesgo </td>
                <td class="u-border-1 u-border-grey-25 u-table-cell">Control </td><td class="u-border-1 u-border-grey-25 u-table-cell">Actividades </td>
                <td class="u-border-1 u-border-grey-25 u-table-cell">Objetivo_del_Control </td><td class="u-border-1 u-border-grey-25 u-table-cell">Responsable_de_Ejecutar_el_Control </td>
                <td class="u-border-1 u-border-grey-25 u-table-cell">Periodo/Fecha_de_Ejecución </td><td class="u-border-1 u-border-grey-25 u-table-cell">Indicador </td>
                <td class="u-border-1 u-border-grey-25 u-table-cell"> </td>
            </tr>
            
                            
                            <tr bgcolor="white">
                            
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                                <select name="tipo" id="tipo" class="form-control" onchange="calcular_vulnerabilidad(event)">
                                <option value="0">Seleccione...</option>
                                <option value="Persona">Persona</option>
                                <option value="Computador">Computador</option>
                                <option value="Servidor">Servidor</option>
                                <option value="SoporteInformatico">Soporte Informatico</option>
                                <option value="RedesyComunicaciones">Redes y Comunicaciones</option>
                                <option value="Aplicacion">Aplicación</option>
                                <option value="Microsoft">Microsoft</option>
                                <option value="BDyArchivos">Bases de Datos y Archivos</option>
                                <option value="ServiciosAuxiliares">Servicios Auxiliares</option>
                                <option value="EquipamientoAuxiliar">Equipamiento Auxiliar</option>
                                <option value="InstalacionesFisicas">Instalaciones Fisicas</option>
                                <option value="IPs">IPs</option>
                            </select>
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <select name="activo" class="form-control" id="activo">
                                <option value="0">Seleccione...</option>
                            </select>
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <input type="text" name="numero" class="form-control">
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <select name="propiedad" class="form-control">
                                <option value="Perdidadeconfidencialidad">Perdida de confidencialidad</option>
                                <option value="Perdidadedisponibilidad">Perdida de disponibilidad</option>
                                <option value="Perdidadeintegridad">Perdida de integridad</option>
                            </select>    
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <input type="text" name="descripcion" class="form-control">
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <select name="dependencia" class="form-control">
                            <%
                            resultado=st.executeQuery("select nom_dependencia from dependencia order by nom_dependencia ASC");
                            while (resultado.next()){

                            %>

                                <option value=<%=resultado.getString(1)%>><%=resultado.getString(1)%></option>

                            <%
                            }   
                            %>

                            </select>
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <select name="amenaza" class="form-control">
                            <%
                            ResultSet resultado2=null;
                            resultado2=st.executeQuery("select nom_amenaza from amenaza");
                            while (resultado2.next()){

                            %>

                                <option value=<%=resultado2.getString(1)%>><%=resultado2.getString(1)%></option>

                            <%
                            }   
                            %>

                            </select>
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <select name="vulnerabilidad" id="vulnerabilidad" class="form-control">
                                                        
                            <option value="0"> </option>

                            </select>
                            </td>
                            
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <select name="probabilidad" class="form-control">
                            <%
                            ResultSet resultado4=null;
                            resultado4=st.executeQuery("select *from probabilidad");
                            while (resultado4.next()){

                            %>

                                <option value=<%=resultado4.getString(1)%>><%=resultado4.getString(2)%></option>
                                
                            <%
                            }   
                            %>

                            </select>
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                                <select name="impacto" class="form-control" onchange="calcular_zona(event)">>
                                <%
                                ResultSet resultado5=null;
                                resultado5=st.executeQuery("select *from impacto");
                                while (resultado5.next()){

                                %>

                                    <option value=<%=resultado5.getString(1)%>><%=resultado5.getString(2)%></option>

                                <%
                                }   
                                %>

                            </select>
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <input type="text" name="zonariesgo" class="form-control">
                            </td>
                            
                            
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <select name="opcionmanejoriesgo" class="form-control">
                                <option value="ReducirelRiesgo">Reducir el Riesgo</option>
                                <option value="AceptarelRiesgo">Aceptar el Riesgo</option>
                                <option value="EvitarelRiesgo">Evitar el Riesgo</option>
                                <option value="CompartirelRiesgo">Compartir el Riesgo</option>
                            </select> 
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <input type="text" name="descripcioncontrol" class="form-control">
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <select name="dependencia2" class="form-control">
                            <%
                            ResultSet resultado6=null;
                            resultado6=st.executeQuery("select nom_dependencia from dependencia order by nom_dependencia ASC");
                            while (resultado6.next()){

                            %>

                                <option value=<%=resultado6.getString(1)%>><%=resultado6.getString(1)%></option>

                            <%
                            }   
                            %>

                            </select>
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <select name="probabilidadresidual" class="form-control">
                            <%
                            ResultSet resultado7=null;
                            resultado7=st.executeQuery("select *from probabilidad");
                            while (resultado7.next()){

                            %>

                                <option value=<%=resultado7.getString(1)%>><%=resultado7.getString(2)%></option>

                            <%
                            }   
                            %>

                            </select>
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <select name="impactoresidual" class="form-control" onchange="calcular_zonaresidual(event)">
                            <%
                            ResultSet resultado8=null;
                            resultado8=st.executeQuery("select *from impacto");
                            while (resultado8.next()){

                            %>

                                <option value=<%=resultado8.getString(1)%>><%=resultado8.getString(2)%></option>

                            <%
                            }   
                            %>

                            </select>
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                                <input type="text" name="zonariesgoresidual" class="form-control">
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <input type="text" name="OpcionManejoRiesgo" class="form-control">
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <input type="text" name="Control" class="form-control">
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <input type="text" name="Actividad" class="form-control">
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <input type="text" name="ObjetivoControl" class="form-control">
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <input type="text" name="ResponsableControl" class="form-control">
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <input type="text" name="Periodo" class="form-control">
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <input type="text" name="Indicador" class="form-control">
                            </td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell">
                            <button class="btn btn-primary" type="submit" name="guardar">Guardar</button>
                            </td>
                            </tr>
                            
                            <%
                                
                            if(request.getParameter("guardar")!=null){
                
                            String tipo=request.getParameter("tipo");
                            String activo=request.getParameter("activo");
                            String numero=request.getParameter("numero");
                            String propiedad=request.getParameter("propiedad");
                            String descripcion=request.getParameter("descripcion");
                            String dependencia=request.getParameter("dependencia");
                            String amenaza=request.getParameter("amenaza");
                            String vulnerabilidad=request.getParameter("vulnerabilidad");
                            String probabilidad=request.getParameter("probabilidad");
                            String impacto=request.getParameter("impacto");
                            String zonariesgo=request.getParameter("zonariesgo");
                            String opcionmanejoriesgo=request.getParameter("opcionmanejoriesgo");
                            String descripcioncontrol=request.getParameter("descripcioncontrol");
                            String dependencia2=request.getParameter("dependencia2");
                            String probabilidadresidual=request.getParameter("probabilidadresidual");
                            String impactoresidual=request.getParameter("impactoresidual");
                            String zonariesgoresidual=request.getParameter("zonariesgoresidual");
                            String opcionmanejoriesgo2=request.getParameter("OpcionManejoRiesgo");
                            String control=request.getParameter("Control");
                            String actividad=request.getParameter("Actividad");
                            String objetivocontrol=request.getParameter("ObjetivoControl");
                            String responsablecontrol=request.getParameter("ResponsableControl");
                            String periodo=request.getParameter("Periodo");
                            String indicador=request.getParameter("Indicador");

                                st.executeUpdate("insert into matrizriesgo values(0,'"+tipo+"','"+activo+"','"+numero
                                        +"','"+propiedad+"','"+descripcion+"','"+dependencia+"','"+amenaza
                                        +"','"+vulnerabilidad+"','"+probabilidad+"','"+impacto+"','"+zonariesgo
                                        +"','"+opcionmanejoriesgo+"','"+descripcioncontrol+"','"+dependencia2+"','"+probabilidadresidual
                                        +"','"+impactoresidual+"','"+zonariesgoresidual+"','"+opcionmanejoriesgo2+"','"+control
                                        +"','"+actividad+"','"+objetivocontrol+"','"+responsablecontrol+"','"+periodo+"','"+indicador+"')");

                            %>
                            <script>
                            //swal("Correcto"," Registro Insertado en MatrizRiesgo", "success");
                            </script>
                            <%
                            }
                                
                        //}
                        st.close();
                        resultado.close();
                        conexion.close();
            }catch(Exception e){}
            
            %> 
            
        </table>
        </form>
            <br>
            <br>
            <br>
            <br>
    <div class="item">
      <img class="imagen-logo" src="Imagenes/LOGO GOBERNACION.jpg" alt="">  
    </div>
    </div>
    
    </div>
     

    <br>
    <br>
    
    <script>
        function calcular_zona(event){
           var probabilidad = (document.getElementsByName("probabilidad")[0].value);
           var impacto = event.target.value;
           var a=document.getElementsByName("zonariesgo")[0];
           var union=probabilidad+impacto;
           if(union=="11" || union=="12" || union=="21" || union=="22" || union=="31"){
           a.value="Zona de Riesgo Baja";
           a.style.backgroundColor = "green";
           }
           else{
           if(union=="13" || union=="23" || union=="32" || union=="41"){
           a.value="Zona de Riesgo Moderada";
           a.style.backgroundColor = "yellow";
           }
           else{
           if(union=="14" || union=="24" || union=="33" || union=="42" || union=="43" || union=="51" || union=="52"){
           a.value="Zona de Riesgo Alta";
           a.style.backgroundColor = "orange";
           }
           else{
           a.value="Zona de Riesgo Extrema";
           a.style.backgroundColor = "red";
           }
           }
           }
        }
    </script>
    
    <script>
        function calcular_zonaresidual(event,num){
           var probabilidadresidual = document.getElementsByName("probabilidadresidual")[0].value;
           var impactoresidual = event.target.value;
           var union=probabilidadresidual+impactoresidual;
           var a=document.getElementsByName("zonariesgoresidual")[0];
           if(union=="11" || union=="12" || union=="21" || union=="22" || union=="31"){
           a.value="Zona de Riesgo Baja";
           a.style.backgroundColor = "green";
           }
           else{
           if(union=="13" || union=="23" || union=="32" || union=="41"){
           a.value="Zona de Riesgo Moderada";
           a.style.backgroundColor = "yellow";
           }
           else{
           if(union=="14" || union=="24" || union=="33" || union=="42" || union=="43" || union=="51" || union=="52"){
           a.value="Zona de Riesgo Alta";
           a.style.backgroundColor = "orange";
           }
           else{
           a.value="Zona de Riesgo Extrema";
           a.style.backgroundColor = "red";
           }
           }
           }
        }
    </script>
    
    
    
    </body>
</html>

