<%-- 
    Document   : BDyArchivos
    Created on : 5/07/2022, 9:11:47 p. m.
    Author     : eliof
--%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BD y Archivos - GOBERNACION DE BOLIVAR</title>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="Estilos3.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.rtl.min.css">
    <link href="/docs/5.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>
    <center><header> 
        <br>
            <center><nav>
                    
                <center id="texto-header-inicio"><b>INVENTARIO- GOBERNACION DE BOLIVAR</b></center>
                
                ROL: DIGITADOR
                    <br> 
                    <br>    
                    <center>
                        <a id="enlaces" href="login.jsp">
                        <button class="boton-navegador-sesion"><b>Cerrar Sesión</b>
                        </button></a>
                        <a id="enlaces" href="BDyArchivos2.jsp">
                            <button class="boton-navegador-sesion" name="buscar"><b>Buscar</b></button>
                        </a>
                        
                    </center>
                
            </nav></center>
    </header></center>
    
   
<div class="container">   
     <div class="d-flex flex-column flex-shrink-0 p-5 text-white bg-dark" style="width: 340px;" class="item" id="item1">
      <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Activos</font></font></span>
    
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">
      <li>
        <a href="Dependencia.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Dependencias
        </font></font></a>
      </li>
      <li>
        <a href="Persona.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Personas
        </font></font></a>
      </li>
      <a href="Microsoft.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#table"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Microsoft
        </font></font></a>
      </li>
      <li  class="nav-item">
        <a href="#" class="nav-link active" aria-current="page">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#grid"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          BD y Archivos
        </font></font></a>
      </li>
      <li>
        <a href="SoportesInformaticos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Soportes Informaticos
        </font></font></a>
      </li>
      <li>
        <a href="RedesyComunicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Redes Comunicaciones
        </font></font></a>
      </li>
      <li>
        <a href="Computadores.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Computadores
        </font></font></a>
      </li>
      <li>
        <a href="Aplicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Aplicaciones
        </font></font></a>
      </li>
      <li>
        <a href="ServiciosAuxiliares.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servicios Auxiliares
        </font></font></a>
      </li>
      <li>
        <a href="EquipamientoAuxiliar.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Equipamiento Auxiliar
        </font></font></a>
      </li>
      <li>
        <a href="Servidores.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servidores
        </font></font></a>
      </li>
      <li>
        <a href="InstalacionesFisicas.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Instalaciones Fisicas
        </font></font></a>
      </li>
      <li>
        <a href="IPs.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          IPs
        </font></font></a>
      </li>
      <li>
        <a href="MapaRiesgos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Mapa de Riesgos
        </font></font></a>
      </li>
      
    </ul>
    
    <hr>
    <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <a id="enlaces" href="ReporteDependencias.jsp"><span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Reportes</font></font></span></a>
    <hr>
   
    
  </div>
    <div class="cuadrado2" >
        <form class="row g-3" style="width: 840px;">
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Id BD o Archivo</label>
                  <input type="text" name="id_bd" class="form-control" required>
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Tipo</label>
                  <select name="tipo" class="form-control">
                    <option value="BD_Estrategicas">BD_Estrategicas</option>
                    <option value="BD_Misionales">BD_Misionales</option>
                    <option value="BD_Apoyo">BD_Apoyo</option>
                    <option value="BD_EvaluacionyMejora">BD_EvaluacionyMejora</option>
                    <option value="Archivo_Estrategicas">Archivo_Estrategicas</option>
                    <option value="Archivo_Misionales">Archivo_Misionales</option>
                    <option value="Archivo_Apoyo">Archivo_Apoyo</option>
                    <option value="Archivo_EvaluacionyMejora">Archivo_EvaluacionyMejora</option>
                  </select>
                 </div></center>
                <br>
                  
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Nombre</label>
                  <input type="text" name="nom" class="form-control" required>
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Dependencia</label>
                  <select name="dependencia" class="form-control">
                <%
                    Connection conexion=null;
                    Statement st=null;
                    ResultSet resultado=null;
                                         
                try{
                Class.forName("com.mysql.jdbc.Driver");
                conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/inventariogob?user=root&password=130646");
                st=conexion.createStatement();
                resultado=st.executeQuery("select nom_dependencia from dependencia");
                while (resultado.next()){
                
                %>
                
                    <option value=<%=resultado.getString(1)%>><%=resultado.getString(1)%></option>
                  
                <%
                }   
                %>
                
                </select>
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Proceso</label>
                    <input type="text" name="proceso" class="form-control">
                </div></center>
                  <br>
                  
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Dbms</label>
                  <input type="text" name="dbms" class="form-control">
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Descripción</label>
                    <input type="text" name="descripcion" class="form-control">
                </div></center>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Ubicación</label>
                    <input type="text" name="ubicacion" class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Clasificación1712</label>
                  <select name="c1712" class="form-control">
                    <option value="InformacionPublica">Informacion Publica</option>
                    <option value="InformacionPublicaClasificada">Informacion Publica Clasificada</option>
                    <option value="InformacionPublicaReservada">Informacion Publica Reservada</option>
                  </select>
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Clasificación1581</label>
                    <input type="text" name="c1581" class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Criticidad de Confidencialidad</label>
                  <select name="cc" class="form-control">
                    <option value="Alta">Alta</option>
                    <option value="Media">Media</option>
                    <option value="Baja">Baja</option>
                  </select>
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Criticidad de Integridad</label>
                    <select name="ci" class="form-control">
                    <option value="Alta">Alta</option>
                    <option value="Media">Media</option>
                    <option value="Baja">Baja</option>
                  </select>
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Criticidad de Disponibilidad</label>
                    <select name="cd" class="form-control">
                    <option value="Alta">Alta</option>
                    <option value="Media">Media</option>
                    <option value="Baja">Baja</option>
                  </select>
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Responsable</label>
                    <input type="text" name="responsable" class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Encargado</label>
                    <input type="text" name="encargado" class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Accesos</label>
                    <input type="text" name="accesos" class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Contrato de Mantenimiento</label>
                    <input type="text" name="cm" class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Componente</label>
                    <input type="text" name="componente" class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Fecha Ingreso(AAAA-MM-DD)</label>
                    <input type="text" name="fec_ing" class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Fecha Salida(AAAA-MM-DD)</label>
                    <input type="text" name="fec_sal" class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Compatibilidad Ipv6</label>
                    <input type="text" name="ipv6" class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Dirección Ip</label>
                    <input type="text" name="ip" class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Servidor</label>
                  <select name="servidor" class="form-control">
                <%
                resultado=st.executeQuery("select nom_servidor from servidor");
                while (resultado.next()){
                
                %>
                
                    <option value=<%=resultado.getString(1)%>><%=resultado.getString(1)%></option>
                  
                <%
                }   
                %>
                
                </select>
                 </div></center>
                <br>
                <br>
                <br>
                <br>
                <br>
                
                <center>
                  <button class="btn btn-primary" type="submit" name="guardar">Guardar</button>
                </center>
        </form>
                </div>
                </div>
                
                <%
            if(request.getParameter("guardar")!=null){
                
            long id_bd=Long.parseLong(request.getParameter("id_bd"));
            String tipo=request.getParameter("tipo");
            String nom=request.getParameter("nom");
            String dependencia=request.getParameter("dependencia");
            String proceso=request.getParameter("proceso");
            String dbms=request.getParameter("dbms");
            String descripcion=request.getParameter("descripcion");
            String ubicacion=request.getParameter("ubicacion");
            String c1712=request.getParameter("c1712");
            String c1585=request.getParameter("c1585");
            String cc=request.getParameter("cc");
            String ci=request.getParameter("ci");
            String cd=request.getParameter("cd");
            String responsable=request.getParameter("responsable");
            String encargado=request.getParameter("encargado");
            String accesos=request.getParameter("accesos");
            String cm=request.getParameter("cm");
            String componente=request.getParameter("componente");
            String fec_ing=request.getParameter("fec_ing");
            String fec_sal=request.getParameter("fec_sal");
            String ipv6=request.getParameter("ipv6");
            String ip=request.getParameter("ip");
            String servidor=request.getParameter("servidor");
            
            ResultSet r4=null;
            r4=st.executeQuery("select id_archivo from bdyarchivo where id_archivo="+id_bd+"");
                
            if(r4.next()){
               %>
            <script>
            swal("Error"," ID Existe en BDyArchivo", "warning");
            </script>
            <% 
            
            }
                ResultSet r5=null;    
                r5=st.executeQuery("select id_servidor from servidor where nom_servidor='"+servidor+"'");
                if(r5.next()){
                    int id_servidor=Integer.parseInt(r5.getString(1));         
                
                
                //st=conexion.createStatement();
                st.executeUpdate("insert into bdyarchivo values("+id_bd+",'"+tipo+"','"+nom+"','"+dependencia+"','"+proceso
                       +"','"+dbms+"','"+descripcion+"','"+ubicacion+"','"+c1712+"','"+c1585+"','"+cc+"','"+ci+"','"+cd+"','"+responsable+"','"+encargado
                       +"','"+accesos+"','"+cm+"','"+componente+"','"+fec_ing+"','"+fec_sal+"','"+ipv6
                       +"','"+ip+"',"+id_servidor+")");
                
                %>
            <script>
            swal("Correcto"," Registro Insertado en BDyArchivo", "success");
            </script>
            <%
                //request.getRequestDispatcher("Dependencia.jsp").forward(request, response);
                conexion.close();
                st.close();
            }
            }               
            }catch(Exception e){
                System.out.println("Conexión Fallida: "+e);
            
            }
            
            %>
        
<br>
    <div class="item">
      <center><img class="imagen-logo" src="Imagenes/LOGO GOBERNACION.jpg" alt=""></center>  
    </div> 

    <br>
    <br>
     
</body>
</html>

