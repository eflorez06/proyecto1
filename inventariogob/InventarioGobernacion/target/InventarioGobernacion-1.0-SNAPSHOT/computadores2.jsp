<%-- 
    Document   : computadores2
    Created on : 27/07/2022, 1:39:40 a. m.
    Author     : eliof
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Computador - GOBERNACION DE BOLIVAR</title>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="Estilos3.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.rtl.min.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>
    <center><header> 
        <br>
            <center><nav>
                    
                <center id="texto-header-inicio"><b>INVENTARIO- GOBERNACION DE BOLIVAR</b></center>
                
                ROL: DIGITADOR
                    <br> 
                    <br>    
                    <center>
                        <a id="enlaces" href="login.jsp">
                        <button class="boton-navegador-sesion"><b>Cerrar Sesión</b>
                        </button></a>             
                    </center>
            </nav></center>
    </header></center>
    
   
<div class="container">   
     <div class="d-flex flex-column flex-shrink-0 p-5 text-white bg-dark" style="width: 340px;" class="item" id="item1">
      <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Activos</font></font></span>
    
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">
      <li>
        <a href="Dependencia.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Dependencias
        </font></font></a>
      </li>
      <li>
        <a href="Persona.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Personas
        </font></font></a>
      </li>
      <a href="Microsoft.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#table"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Microsoft
        </font></font></a>
      </li>
      <li>
        <a href="BDyArchivos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#grid"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          BD y Archivos
        </font></font></a>
      </li>
      <li>
        <a href="SoportesInformaticos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Soportes Informaticos
        </font></font></a>
      </li>
      <li>
        <a href="RedesyComunicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Redes Comunicaciones
        </font></font></a>
      </li>
      <li class="nav-item">
        <a href="#" class="nav-link active" aria-current="page">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Computadores
        </font></font></a>
      </li>
      <li>
        <a href="Aplicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Aplicaciones
        </font></font></a>
      </li>
      <li>
        <a href="ServiciosAuxiliares.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servicios Auxiliares
        </font></font></a>
      </li>
      <li>
        <a href="EquipamientoAuxiliar.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Equipamiento Auxiliar
        </font></font></a>
      </li>
      <li>
        <a href="Servidores.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servidores
        </font></font></a>
      </li>
      <li>
        <a href="InstalacionesFisicas.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Instalaciones Fisicas
        </font></font></a>
      </li>
      <li>
        <a href="IPs.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          IPs
        </font></font></a>
      </li>
      <li>
        <a href="MapaRiesgos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Mapa de Riesgos
        </font></font></a>
      </li>
      
    </ul>
    
    <hr>
    <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <a id="enlaces" href="ReporteDependencias.jsp"><span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Reportes</font></font></span></a>
    <hr>
   
    
  </div>
    
    <div style="width: 840px;">
    <form style="width: 840px;">
        <center><div class="col-md-4">
                <label for="validationServer01" class="form-label">Id Computador</label>
                  <input type="text" name="id" class="form-control is-valid" id="validationServer01" required>
        </div></center>
                <br>
                
                <center><button class="btn btn-primary" type="submit" name="buscar">Buscar</button></center>
        </center>
    </form>
    
    <%
            String id=request.getParameter("id");
            
            
            Connection conexion=null;
            Statement st=null;
            ResultSet resultado=null;
            
            try{
            Class.forName("com.mysql.jdbc.Driver");
            conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/inventariogob?user=root&password=130646");
            st=conexion.createStatement();
            
            
               resultado=st.executeQuery("select *from computador where id_computador="+id+"");
               while(resultado.next()){
        
    %>
                <br>
                <br>
    <form>
    
    
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Id Computador</label>
                  <input type="text" name="id_pc" value=<%=resultado.getString(1)%> class="form-control" required>
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Tipo</label>
                  <select name="tipo_pc" value=<%=resultado.getString(2)%> class="form-control">
                    <option value="PC_SecretaríasEstrategicas">PC_SecretaríasEstrategicas</option>
                    <option value="PC_SecretaríasMisionales">PC_SecretaríasMisionales</option>
                    <option value="PC_SecretaríasApoyo">PC_SecretaríasApoyo</option>
                    <option value="PC_SecretaríasEvaluacionyMejora">PC_SecretaríasEvaluacionyMejora</option>
                    <option value="Portatil_SecretaríasEstrategicas">Portatil_SecretaríasEstrategicas</option>
                    <option value="Portatil_SecretaríasMisionales">Portatil_SecretaríasMisionales</option>
                    <option value="Portatil_SecretaríasApoyo">Portatil_SecretaríasApoyo</option>
                    <option value="Portatil_SecretaríasEvaluacionyMejora">Portatil_SecretaríasEvaluacionyMejora</option>
                  </select>
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Nombre</label>
                    <input type="text" name="nom_pc" value=<%=resultado.getString(3)%> class="form-control" required>
                </div></center>
                  <br>
                  
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Modelo</label>
                    <input type="text" name="modelo_pc" value=<%=resultado.getString(4)%> class="form-control" >
                </div></center>
                  <br>
                  
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Estado</label>
                  <input type="text" name="estado_pc" value=<%=resultado.getString(5)%> class="form-control" >
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Serie</label>
                    <input type="text" name="serie_pc" value=<%=resultado.getString(6)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Sistema Operativo</label>
                    <input type="text" name="so_pc" value=<%=resultado.getString(7)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Ubicación PC</label>
                    <input type="text" name="ubicacion_pc" value=<%=resultado.getString(8)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Procesador</label>
                    <input type="text" name="cpu_pc" value=<%=resultado.getString(9)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Grupo</label>
                    <input type="text" name="grupo_pc" value=<%=resultado.getString(10)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Dominio</label>
                    <input type="text" name="dominio_pc" value=<%=resultado.getString(11)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Fabricante</label>
                    <input type="text" name="fabricante_pc" value=<%=resultado.getString(12)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Fecha Ingreso(AAAA-MM-DD)</label>
                    <input type="text" name="fec_ing_pc" value=<%=resultado.getString(13)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Fecha Salida(AAAA-MM-DD)</label>
                    <input type="text" name="fec_sal_pc" value=<%=resultado.getString(14)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Memoria</label>
                    <input type="text" name="ram_pc" value=<%=resultado.getString(15)%> class="form-control"  >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Dirección IP</label>
                    <input type="text" name="ip_pc" value=<%=resultado.getString(16)%> class="form-control"  >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Marca</label>
                    <input type="text" name="marca_pc" value=<%=resultado.getString(17)%> class="form-control"  >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">UUID</label>
                    <input type="text" name="uuid_pc" value=<%=resultado.getString(18)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Usuario</label>
                    <input type="text" name="usuario_pc" value=<%=resultado.getString(19)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Antivirus</label>
                    <input type="text" name="antivirus_pc" value=<%=resultado.getString(20)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Tipo Disco Duro</label>
                    <input type="text" name="dd_pc" value=<%=resultado.getString(21)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Dependencia</label>
                    <input type="text" name="id_dep" value=<%=resultado.getString(22)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Descripción</label>
                    <input type="text" name="descripcion_pc" value=<%=resultado.getString(23)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Clasificación</label>
                    <input type="text" name="clasificacion_pc" value=<%=resultado.getString(24)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Software Instalado</label>
                    <input type="text" name="si_pc" value=<%=resultado.getString(25)%> class="form-control" >
                </div></center>
                <br>
                
                 <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Versión Office</label>
                    <input type="text" name="office_pc" value=<%=resultado.getString(26)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Criticidad</label>
                  <select name="criticidad_pc" value=<%=resultado.getString(27)%> class="form-control">
                    <option value="Alta">Alta</option>
                    <option value="Media">Media</option>
                    <option value="Baja">Baja</option>
                  </select>
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Responsable</label>
                    <input type="text" name="reponsable_pc" value=<%=resultado.getString(28)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Accesos</label>
                    <input type="text" name="accesos_pc" value=<%=resultado.getString(29)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Contrato de Mantenimiento</label>
                    <input type="text" name="cm_pc" value=<%=resultado.getString(30)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Compatibilidad IPv6</label>
                    <input type="text" name="ipv6_pc" value=<%=resultado.getString(31)%> class="form-control" >
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Rol</label>
                    <input type="text" name="rol_pc" value=<%=resultado.getString(32)%> class="form-control" >
                </div></center>
                <br>
                <br>
                <br>
                <br>
                
                <center>
                  
                  <button class="btn btn-primary" type="submit" name="actualizar">Actualizar</button>
                  <button class="btn btn-primary" type="submit" name="eliminar">Eliminar</button>
                 
                </center>
                
        <%      
        }
        if(request.getParameter("actualizar")!=null){
                
            long id_pc=Long.parseLong(request.getParameter("id_pc"));
            String tipo_pc=request.getParameter("tipo_pc");
            String nom_pc=request.getParameter("nom_pc");
            String modelo_pc=request.getParameter("modelo_pc");
            String estado_pc=request.getParameter("estado_pc");
            String serie_pc=request.getParameter("serie_pc");
            String so_pc=request.getParameter("so_pc");
            String ubicacion_pc=request.getParameter("ubicacion_pc");
            String cpu_pc=request.getParameter("cpu_pc");
            String grupo_pc=request.getParameter("grupo_pc");
            String dominio_pc=request.getParameter("dominio_pc");
            String fabricante_pc=request.getParameter("fabricante_pc");
            String fec_ing_pc=request.getParameter("fec_ing_pc");
            String fec_sal_pc=request.getParameter("fec_sal_pc");
            String ram_pc=request.getParameter("ram_pc");
            String ip_pc=request.getParameter("ip_pc");
            String marca_pc=request.getParameter("marca_pc");
            String uuid_pc=request.getParameter("uuid_pc");
            String usuario_pc=request.getParameter("usuario_pc");
            String antivirus_pc=request.getParameter("antivirus_pc");
            String dd_pc=request.getParameter("dd_pc");
            String id_dep=request.getParameter("id_dep");
            String descripcion_pc=request.getParameter("descripcion_pc");
            String clasificacion_pc=request.getParameter("clasificacion_pc");
            String si_pc=request.getParameter("si_pc");
            String office=request.getParameter("office");
            String criticidad_pc=request.getParameter("criticidad_pc");
            String responsable_pc=request.getParameter("responsable_pc");
            String accesos_pc=request.getParameter("accesos_pc");
            String cm_pc=request.getParameter("cm_pc");
            String ipv6_pc=request.getParameter("ipv6_pc");
            String rol_pc=request.getParameter("rol_pc");
            
            st.executeUpdate("update computador set tipo_computador='"+tipo_pc
                                +"',nom_computador='"+nom_pc+"',modelo_computador='"+modelo_pc+"',estado_computador='"+estado_pc
                                +"',serie_computador='"+serie_pc+"',so_computador='"+so_pc+"',ubicacion_computador='"+ubicacion_pc
                                +"',procesador_computador='"+cpu_pc+"',grupo_computador='"+grupo_pc+"',dominio_computador='"+dominio_pc
                                +"',fabricante_computador='"+fabricante_pc+"',fec_ing_activo_computador='"+fec_ing_pc+"',fec_sal_activo_computador='"+fec_sal_pc
                                +"',memoria_computador='"+ram_pc+"',direccion_ip_servidor='"+ip_pc+"',marca_computador='"+marca_pc
                                +"',uuid_computador='"+uuid_pc+"',usuario_computador='"+usuario_pc+"',antivirus_computador='"+antivirus_pc
                                +"',tipo_disco_duro_computador='"+dd_pc+"',dependencia_id_dependencia='"+id_dep+"',descripcion_computador='"+descripcion_pc
                                +"',clasificion_computador='"+clasificacion_pc+"',software_instalado_computador='"+si_pc+"',version_office_computador='"+office
                                +"',criticidad_computador='"+criticidad_pc+"',responsable_computador='"+responsable_pc+"',accesos_computador='"+accesos_pc
                                +"',contrato_mantenimiento_computador='"+cm_pc+"',compatibilidadipv6_computador='"+ipv6_pc
                                +"',rol_computador='"+rol_pc+"' where id_computador="+id_pc+"");
            %>
            <script>
            swal("Correcto"," Registro Actualizado en Computador", "success");
            </script>
            <%
                //request.getRequestDispatcher("Dependencia.jsp").forward(request, response);
                conexion.close();
                st.close();
            }
            else{
            if(request.getParameter("eliminar")!=null){
                long id_pc=Long.parseLong(request.getParameter("id_pc"));
                st.executeUpdate("delete from computador where id_computador="+id_pc+"");
                %>
            <script>
            swal("Correcto"," Registro Eliminado en Computador", "success");
            </script>
            <%
            }

            }                
            }catch(Exception e){
                System.out.println("Conexión Fallida: "+e);
            
            }
            
            %>
                
        </form>
    </div>
</div> 
<br>
    <div class="item">
      <center><img class="imagen-logo" src="Imagenes/LOGO GOBERNACION.jpg" alt=""></center>  
    </div> 

    <br>
    <br>
     
</body>
</html>
