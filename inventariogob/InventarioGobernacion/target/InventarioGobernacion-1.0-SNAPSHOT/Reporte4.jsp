<%-- 
    Document   : Reporte4
    Created on : 1/08/2022, 11:09:18 p. m.
    Author     : eliof
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reporte4 - GOBERNACION DE BOLIVAR</title>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="Estilos3.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.rtl.min.css">
    <link href="/docs/5.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    
</head>
<body>
    <center><header> 
        <br>
            <center><nav>
                    
                <center id="texto-header-inicio"><b>INVENTARIO- GOBERNACION DE BOLIVAR</b></center>
                
                ROL:General - Sesión Iniciada 
                    <br> 
                    <br>    
                    <center>
                        <a id="enlaces" href="login.jsp">
                        <button class="boton-navegador-sesion"><b>Cerrar Sesión</b>
                        </button></a>
                        <a id="enlaces" href="SoportesInformaticos2.jsp">
                            <button class="boton-navegador-sesion" name="buscar"><b>Buscar</b></button></a>
                        
                        
                    </center>
                
            </nav></center>
    </header></center>
    
   
<div class="container">   
     <div class="d-flex flex-column flex-shrink-0 p-5 text-white bg-dark" style="width: 340px;" class="item" id="item1">
      <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Reportes</font></font></span>
    
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">
      <li class="nav-item">
        <a href="ReporteDependencias.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Dependencias
        </font></font></a>
      </li>
      <li class="nav-item">
        <a href="Reporte1.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Equipos
        </font></font></a>
      </li>
      <li>
        <a href="Reporte2.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Eq. por Dependencia
        </font></font></a>
      </li>
      <li>
        <a href="Reporte3.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#table"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          BD por C-1712
        </font></font></a>
      </li>
      <li>
        <a href="#" class="nav-link active" aria-current="page">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#grid"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Imp. por Dependencia
        </font></font></a>
      </li>
      
    </ul>
    
    <hr>
    <a id="enlaces" href="Dependencia.jsp"><span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Inicio</font></font></span></a>
  </div>
     
        
    <div class="cuadrado2">
        <form style="width: 840px;">
        <center><div class="col-md-4">
                <label for="validationServer01" class="form-label">Dependencia</label>
                  <select name="dependencia" class="form-control is-valid">
                <%
                    Connection conexion=null;
                    Statement st=null;
                    ResultSet resultado=null;
                                         
                try{
                Class.forName("com.mysql.jdbc.Driver");
                conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/inventariogob?user=root&password=130646");
                st=conexion.createStatement();
                resultado=st.executeQuery("select nom_dependencia from dependencia");
                while (resultado.next()){
                
                %>
                
                    <option value=<%=resultado.getString(1)%>><%=resultado.getString(1)%></option>
                  
                <%
                }   
                %>
                </select>
                 </div></center>
                <br>
                
                <center><button class="btn btn-primary" type="submit" name="buscar">Buscar</button></center>
        </center>
    </form>
          <br>
          <br>
        
        <table class="table table-bordered" border="1" width="800" text align="center">
            
            <tr bgcolor="skyblue">
                <td class="u-border-1 u-border-grey-25 u-table-cell" colspan="6"><center><b>Listado Impresoras</b></center></td>
            </tr>
            <tr bgcolor="skyblue">
                <td class="u-border-1 u-border-grey-25 u-table-cell">Id </td><td class="u-border-1 u-border-grey-25 u-table-cell">Dependencia </td>
                <td class="u-border-1 u-border-grey-25 u-table-cell">Tipo </td><td class="u-border-1 u-border-grey-25 u-table-cell">Nombre </td>
                <td class="u-border-1 u-border-grey-25 u-table-cell">Marca </td><td class="u-border-1 u-border-grey-25 u-table-cell">Modelo </td>                
            </tr>
    
    <%
           String dependencia=request.getParameter("dependencia");
            
            ResultSet r2=null;
            r2=st.executeQuery("select id_dependencia from dependencia where nom_dependencia='"+dependencia+"'");
            if(r2.next()){
            String id=r2.getString(1);
            
            
               resultado=st.executeQuery("select id_soporte_informatico,Dependencia_id_dependencia,tipo_soporte_informatico,nom_soporte_informatico,marca_soporte_informatico,modelo_soporte_informatico from soportesinformaticos where Dependencia_id_dependencia='"+id+"' and tipo_soporte_informatico='Impresora' order by id_soporte_informatico ASC");
               while(resultado.next()){
        
    %>
                          
                            <tr bgcolor="white">
                            <td class="u-border-1 u-border-grey-25 u-table-cell"><%=resultado.getString(1)%></td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell"><%=resultado.getString(2)%></td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell"><%=resultado.getString(3)%></td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell"><%=resultado.getString(4)%></td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell"><%=resultado.getString(5)%></td>
                            <td class="u-border-1 u-border-grey-25 u-table-cell"><%=resultado.getString(6)%></td>
                            </tr>
                            <%
                        }
                        st.close();
                        resultado.close();
                        conexion.close();
            }
            }catch(Exception e){}
            
            %> 
        </table>
    </div>
    </div>
    <div class="item">
      <center><img class="imagen-logo" src="Imagenes/LOGO GOBERNACION.jpg" alt=""></center>  
    </div> 

    <br>
    <br>
    </body>
</html>
