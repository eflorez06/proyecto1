<%-- 
    Document   : BDyArchivos2
    Created on : 30/07/2022, 12:44:39 a. m.
    Author     : eliof
--%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BD y Archivos - GOBERNACION DE BOLIVAR</title>
    <link rel="stylesheet" href="estilos.css">
    <link rel="stylesheet" href="Estilos3.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-grid.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.rtl.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/bootstrap-5.0.0-beta2-dist/css/bootstrap-utilities.rtl.min.css">
    <link href="/docs/5.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>
    <center><header> 
        <br>
            <center><nav>
                    
                <center id="texto-header-inicio"><b>INVENTARIO- GOBERNACION DE BOLIVAR</b></center>
                
                ROL: DIGITADOR
                    <br> 
                    <br>    
                    <center>
                        <a id="enlaces" href="login.jsp">
                        <button class="boton-navegador-sesion"><b>Cerrar Sesión</b>
                        </button></a>             
                    </center>
                    </center>
                
            </nav></center>
    </header></center>
    
   
<div class="container">   
     <div class="d-flex flex-column flex-shrink-0 p-5 text-white bg-dark" style="width: 340px;" class="item" id="item1">
      <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Activos</font></font></span>
    
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">
      <li>
        <a href="Dependencia.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Dependencias
        </font></font></a>
      </li>
      <li>
        <a href="Persona.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Personas
        </font></font></a>
      </li>
      <a href="Microsoft.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#table"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Microsoft
        </font></font></a>
      </li>
      <li  class="nav-item">
        <a href="BDyArchivos.jsp" class="nav-link active" aria-current="page">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#grid"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          BD y Archivos
        </font></font></a>
      </li>
      <li>
        <a href="SoportesInformaticos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Soportes Informaticos
        </font></font></a>
      </li>
      <li>
        <a href="RedesyComunicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Redes Comunicaciones
        </font></font></a>
      </li>
      <li>
        <a href="Computadores.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Computadores
        </font></font></a>
      </li>
      <li>
        <a href="Aplicaciones.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Aplicaciones
        </font></font></a>
      </li>
      <li>
        <a href="ServiciosAuxiliares.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servicios Auxiliares
        </font></font></a>
      </li>
      <li>
        <a href="EquipamientoAuxiliar.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Equipamiento Auxiliar
        </font></font></a>
      </li>
      <li>
        <a href="Servidores.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Servidores
        </font></font></a>
      </li>
      <li>
        <a href="InstalacionesFisicas.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Instalaciones Fisicas
        </font></font></a>
      </li>
      <li>
        <a href="IPs.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          IPs
        </font></font></a>
      </li>
      <li>
        <a href="MapaRiesgos.jsp" class="nav-link text-white">
          <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
          Mapa de Riesgos
        </font></font></a>
      </li>
      
    </ul>
    
   <hr>
    <svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
      <a id="enlaces" href="ReporteDependencias.jsp"><span class="fs-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Reportes</font></font></span></a>
    <hr>
   
    
  </div>
    
    <div>
        <form style="width: 840px;">
        <center><div class="col-md-4">
                <label for="validationServer01" class="form-label">BD o Archivo</label>
                  <select name="nom_archivo" class="form-control is-valid">    
            <%
            Connection conexion=null;
            Statement st=null;
            ResultSet resultado=null;
            
            try{
            Class.forName("com.mysql.jdbc.Driver");
            conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/inventariogob?user=root&password=130646");
            st=conexion.createStatement();
                ResultSet r2=st.executeQuery("select nom_archivo from bdyarchivo");
                while (r2.next()){
                
                %>
                
                    <option value=<%=r2.getString(1)%>><%=r2.getString(1)%></option>
                  
                <%
                }   
                %>
                
                </select>
                 </div></center>
                <br>
                
                <center><button class="btn btn-primary" type="submit" name="buscar">Buscar</button></center>
        </center>
    </form>
    
    <%
            String nom_archivo=request.getParameter("nom_archivo");
            
               resultado=st.executeQuery("select *from bdyarchivo where nom_archivo='"+nom_archivo+"'");
               while(resultado.next()){
        
    %>
                <br>
                <br>
    <form style="width: 840px;">
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Id BD o Archivo</label>
                  <input type="text" name="id_bd" value=<%=resultado.getString(1)%> class="form-control" readonly="readonly">
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Tipo</label>
                    <select name="tipo" value=<%=resultado.getString(2)%> class="form-control">
                    <option value="BD_Estrategicas">BD_Estrategicas</option>
                    <option value="BD_Misionales">BD_Misionales</option>
                    <option value="BD_Apoyo">BD_Apoyo</option>
                    <option value="BD_EvaluacionyMejora">BD_EvaluacionyMejora</option>
                    <option value="Archivo_Estrategicas">Archivo_Estrategicas</option>
                    <option value="Archivo_Misionales">Archivo_Misionales</option>
                    <option value="Archivo_Apoyo">Archivo_Apoyo</option>
                    <option value="Archivo_EvaluacionyMejora">Archivo_EvaluacionyMejora</option>
                  </select></div></center>
                  <br>
                  
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Nombre</label>
                  <input type="text" name="nom" value=<%=resultado.getString(3)%> class="form-control" required>
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Dependencia</label>
                  <input type="text" name="dependencia" value=<%=resultado.getString(4)%> class="form-control">
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Proceso</label>
                  <input type="text" name="proceso" value=<%=resultado.getString(5)%> class="form-control">
                 </div></center>
                <br>
                  
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Dbms</label>
                  <input type="text" name="dbms" value=<%=resultado.getString(6)%> class="form-control">
                 </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Descripción</label>
                    <input type="text" name="descripcion" value=<%=resultado.getString(7)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Ubicación</label>
                    <input type="text" name="ubicacion" value=<%=resultado.getString(8)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Clasificación1712</label>
                    <select name="c1712" value=<%=resultado.getString(9)%> class="form-control">
                    <option value="InformacionPublica">Informacion Publica</option>
                    <option value="InformacionPublicaClasificada">Informacion Publica Clasificada</option>
                    <option value="InformacionPublicaReservada">Informacion Publica Reservada</option>
                    </select>
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Clasificación1581</label>
                    <input type="text" name="c1581" value=<%=resultado.getString(10)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Criticidad de Confidencialidad</label>
                  <select name="cc" value=<%=resultado.getString(11)%> class="form-control">
                    <option value="Alta">Alta</option>
                    <option value="Media">Media</option>
                    <option value="Baja">Baja</option>
                  </select>
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Criticidad de Integridad</label>
                    <select name="ci" value=<%=resultado.getString(12)%> class="form-control">
                    <option value="Alta">Alta</option>
                    <option value="Media">Media</option>
                    <option value="Baja">Baja</option>
                  </select>
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Criticidad de Disponibilidad</label>
                    <select name="cd" value=<%=resultado.getString(13)%> class="form-control">
                    <option value="Alta">Alta</option>
                    <option value="Media">Media</option>
                    <option value="Baja">Baja</option>
                  </select>
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Responsable</label>
                    <input type="text" name="responsable" value=<%=resultado.getString(14)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Encargado</label>
                    <input type="text" name="encargado" value=<%=resultado.getString(15)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Accesos</label>
                    <input type="text" name="accesos" value=<%=resultado.getString(16)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Contrato de Mantenimiento</label>
                    <input type="text" name="cm" value=<%=resultado.getString(17)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Componente</label>
                    <input type="text" name="componente" value=<%=resultado.getString(18)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Fecha Ingreso(AAAA-MM-DD)</label>
                    <input type="text" name="fec_ing" value=<%=resultado.getString(19)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Fecha Salida(AAAA-MM-DD)</label>
                    <input type="text" name="fec_sal" value=<%=resultado.getString(20)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Compatibilidad Ipv6</label>
                    <input type="text" name="ipv6" value=<%=resultado.getString(21)%> class="form-control">
                </div></center>
                <br>
                
                <center><div class="col-md-4">
                    <label for="validationServer01" class="form-label">Dirección Ip</label>
                    <input type="text" name="ip" value=<%=resultado.getString(22)%> class="form-control">
                </div></center>
                <br>
                
                
                
                <center><div class="col-md-4">
                  <label for="validationServer01" class="form-label">Servidor</label>
                  <input type="text" name="servidor" value=<%=resultado.getString(23)%> class="form-control">
                </div></center>
                <br>
                <br>
                <br>
                <br>
                <br>
                
                 <center>
                    <button class="btn btn-primary" type="submit" name="actualizar">Actualizar</button>
                    <button class="btn btn-primary" type="submit" name="eliminar">Eliminar</button>
                </center>
                </form>
    </div>
</div>  
            <%
            }
            if(request.getParameter("actualizar")!=null){
                
            long id_bd=Long.parseLong(request.getParameter("id_bd"));
            String tipo=request.getParameter("tipo");
            String nom=request.getParameter("nom");
            String dependencia=request.getParameter("dependencia");
            String proceso=request.getParameter("proceso");
            String dbms=request.getParameter("dbms");
            String descripcion=request.getParameter("descripcion");
            String ubicacion=request.getParameter("ubicacion");
            String c1712=request.getParameter("c1712");
            String c1581=request.getParameter("c1581");
            String cc=request.getParameter("cc");
            String ci=request.getParameter("ci");
            String cd=request.getParameter("cd");
            String responsable=request.getParameter("responsable");
            String encargado=request.getParameter("encargado");
            String accesos=request.getParameter("accesos");
            String cm=request.getParameter("cm");
            String componente=request.getParameter("componente");
            String fec_ing=request.getParameter("fec_ing");
            String fec_sal=request.getParameter("fec_sal");
            String ipv6=request.getParameter("ipv6");
            String ip=request.getParameter("ip");
            long servidor=Long.parseLong(request.getParameter("servidor"));                 
                
                st.executeUpdate("update bdyarchivo set tipo_archivo='"+tipo+"',nom_archivo='"+nom+"',dependencia='"+dependencia+"',proceso_archivo='"+proceso
                       +"',dbms='"+dbms+"',descripcion='"+descripcion+"',ubicacion_archivo='"+ubicacion+"',clasificacion1712_archivo='"+c1712+"',clasificacion1581_archivo='"+c1581+"',criticidad_confidencialidad_archivo='"+cc+"',criticidad_integridad_archivo='"+ci+"',criticidad_disponibilidad_archivo='"+cd+"',responsable_archivo='"+responsable+"',encargado_archivo='"+encargado
                       +"',accesos_archivo='"+accesos+"',contrato_de_mantenimiento='"+cm+"',componente_archivo='"+componente+"',fec_ing_activo_archivo='"+fec_ing+"',fec_sal_activo_archivo='"+fec_sal+"',compatibilidadipv6_archivo='"+ipv6
                       +"',direccion_ip_archivo='"+ip+"',Servidor_id_servidor="+servidor+" where id_archivo="+id_bd+"");
                %>
            <script>
            swal("Correcto"," Registro Actualizado en BDyArchivo", "success");
            </script>
            <%
                //request.getRequestDispatcher("Dependencia.jsp").forward(request, response);
                conexion.close();
                st.close();
            }
            
            
            else{
            if(request.getParameter("eliminar")!=null){
                long id_bd=Long.parseLong(request.getParameter("id_bd"));
                st.executeUpdate("delete from bdyarchivo where id_archivo="+id_bd+"");
                %>
            <script>
            swal("Correcto"," Registro Eliminado en BDyArchivo", "success");
            </script>
            <%
            }

            }                 
            }catch(Exception e){
                System.out.println("Conexión Fallida: "+e);
            
            }
            %>
               
        
    

<br>
    <div class="item">
      <center><img class="imagen-logo" src="Imagenes/LOGO GOBERNACION.jpg" alt=""></center>  
    </div> 

    <br>
    <br>
     
</body>
</html>
