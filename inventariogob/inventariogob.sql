-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: localhost    Database: inventariogob
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `amenaza`
--

DROP TABLE IF EXISTS `amenaza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `amenaza` (
  `id_amenaza` int NOT NULL AUTO_INCREMENT,
  `nom_amenaza` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id_amenaza`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amenaza`
--

LOCK TABLES `amenaza` WRITE;
/*!40000 ALTER TABLE `amenaza` DISABLE KEYS */;
INSERT INTO `amenaza` VALUES (1,'Hurto_de_información'),(2,'Incumplimiento_en_el_mantenimiento_del_sistema_de_información'),(3,'Destrucción_de_equipos_o_de_medios'),(4,'Polvo,corrosión,congelamiento'),(5,'RadiaciónElectromagnética'),(6,'Error_en_el_uso'),(7,'Pérdida_del_suministro_de_energía'),(8,'FenómenosMetereológicos'),(9,'Hurto_de_medios_o_documentos'),(10,'Negociación_de_acciones'),(11,'EscuchaEncubierta'),(12,'Falla_del_equipo_de_telecomunicaciones'),(13,'Falsificación_de_derechos'),(14,'EspionajeRemoto'),(15,'Saturación_del_sistema_de_información'),(16,'Uso_no_autorizado_del_equipo'),(17,'Abuso_de_los_derechos'),(18,'Corrupcio_de_datos'),(19,'Error_en_el_uso'),(20,'Mal_funcionamiento_del_software'),(21,'Datos_provenientes_de_fuentes_no_confiables'),(22,'Falla_del_equipo'),(23,'Falla_en_los_sistemas');
/*!40000 ALTER TABLE `amenaza` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aplicacion`
--

DROP TABLE IF EXISTS `aplicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aplicacion` (
  `id_aplicacion` int NOT NULL,
  `tipo_aplicacion` varchar(45) NOT NULL,
  `nom_aplicacion` varchar(45) NOT NULL,
  `proceso_aplicacion` varchar(45) DEFAULT NULL,
  `numero_licencias_adquiridas` int DEFAULT NULL,
  `cantidad_licencias_asignadas` int DEFAULT NULL,
  `ubicacion_archivo` varchar(45) DEFAULT NULL,
  `clasificacion_aplicacion` varchar(45) DEFAULT NULL,
  `criticidad_aplicacion` varchar(45) DEFAULT NULL,
  `responsable_aplicacion` varchar(45) DEFAULT NULL,
  `encargado_aplicacion` varchar(45) DEFAULT NULL,
  `accesos_aplicacion` varchar(45) DEFAULT NULL,
  `contrato_de_mantenimiento_aplicacion` varchar(45) DEFAULT NULL,
  `componente_archivo` varchar(45) DEFAULT NULL,
  `fec_ing_activo_aplicacion` date DEFAULT NULL,
  `fec_sal_activo_aplicacion` date DEFAULT NULL,
  `compatibilidadipv6_archivo` varchar(45) DEFAULT NULL,
  `direccion_ip_archivo` varchar(45) DEFAULT NULL,
  `Servidor_id_servidor` int NOT NULL,
  PRIMARY KEY (`id_aplicacion`),
  KEY `fk_Aplicacion_Servidor1_idx` (`Servidor_id_servidor`),
  CONSTRAINT `fk_Aplicacion_Servidor1` FOREIGN KEY (`Servidor_id_servidor`) REFERENCES `servidor` (`id_servidor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aplicacion`
--

LOCK TABLES `aplicacion` WRITE;
/*!40000 ALTER TABLE `aplicacion` DISABLE KEYS */;
INSERT INTO `aplicacion` VALUES (1000,'ClienteServidorSecretariasEstrategicas','spe','ugg',200,100,'jhggjgb','jhgghghg','Alta','50','jgghg','yff','ugig','iuug','2015-10-10','2030-10-10','hgghg','uyugiug',302),(1001,'web','inventario','tyyutuytyuuytgf',100,50,'fhfhfhj','yyyurfuyf','yuyuyu','500','hjgghjgj','yuhgfugfu','yugygyu','yugu','2022-07-29','2030-12-31','hgffhgfghf','yuyuyu',302),(1002,'web','acc','huhijh',100,70,'ghg','hgghh','jjgjk','100','hjhjghg','hjhhj','jhgghj','hh','2015-10-15','2030-10-11','uyghg','ygygu',302),(1003,'web','ada','hjhjk',500,200,'ghhgjhg','hghjghj','hghhj','45','ghghjg','gghghj','hghhjg','hgg','2020-12-16','2030-10-19','ghjhghjg','jjgjg',302),(1004,'web','navega','administracion',50,20,'hghjg','gghghg','hghghjg','10','ugghjg','hghghg','ghgh','hghgg','2015-10-15','2030-10-31','hghghg','hghghjg',302),(1005,'web','bop','uhjjkhkj',12,5,'ghghg','hghfgf','hffghf','25','ghjghg','hhhjj','hgfhj','gghug','2020-12-15','2030-10-10','ggghghj','ghghgjh',302),(1006,'cs','anfitrion','fhfjfhjf',20,10,'jggyug','uyfyufyufg','utytyut','75','yutyyugu','uytgygyug','iutuiguipg','iuuuipgupigp','2020-12-15','2030-10-31','gggug','ugugupi',302),(1007,'ClienteServidorSecretariasEstrategicas','s','s',50,50,'s','s','Alta','s','s','s','s','s','2022-07-29','2030-10-31','s','s',301);
/*!40000 ALTER TABLE `aplicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bdyarchivo`
--

DROP TABLE IF EXISTS `bdyarchivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bdyarchivo` (
  `id_archivo` int NOT NULL,
  `tipo_archivo` varchar(45) NOT NULL,
  `nom_archivo` varchar(45) NOT NULL,
  `dependencia` varchar(45) DEFAULT NULL,
  `proceso_archivo` varchar(45) DEFAULT NULL,
  `dbms` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `ubicacion_archivo` varchar(45) DEFAULT NULL,
  `clasificacion1712_archivo` varchar(45) DEFAULT NULL,
  `clasificacion1581_archivo` varchar(45) DEFAULT NULL,
  `criticidad_confidencialidad_archivo` varchar(45) DEFAULT NULL,
  `criticidad_integridad_archivo` varchar(45) DEFAULT NULL,
  `criticidad_disponibilidad_archivo` varchar(45) DEFAULT NULL,
  `responsable_archivo` varchar(50) DEFAULT NULL,
  `encargado_archivo` varchar(45) DEFAULT NULL,
  `accesos_archivo` varchar(45) DEFAULT NULL,
  `contrato_de_mantenimiento` varchar(45) DEFAULT NULL,
  `componente_archivo` varchar(45) DEFAULT NULL,
  `fec_ing_activo_archivo` date DEFAULT NULL,
  `fec_sal_activo_archivo` date DEFAULT NULL,
  `compatibilidadipv6_archivo` varchar(45) DEFAULT NULL,
  `direccion_ip_archivo` varchar(45) DEFAULT NULL,
  `Servidor_id_servidor` int NOT NULL,
  PRIMARY KEY (`id_archivo`,`Servidor_id_servidor`),
  KEY `fk_BD Y ARCHIVO_Servidor1_idx` (`Servidor_id_servidor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bdyarchivo`
--

LOCK TABLES `bdyarchivo` WRITE;
/*!40000 ALTER TABLE `bdyarchivo` DISABLE KEYS */;
INSERT INTO `bdyarchivo` VALUES (500,'BD_Estrategicas','oracle','Logistica','yiyiyoiyo','oracle','jgjgjgkjggkjgk','jggjkgjkg','InformacionPublica','null','Alta','Alta','Alta','Juan','jkgjggg','ugjgjgjg','jghgjgkjg','jgjhgjkg','2020-12-12','2030-10-31','jhygh','ggjkgkj',302),(501,'BD_Misionales','sqlserver','Tics','gjkgjkgkjg','jkgjkg','jkgjgkj','jgjgkj','Selección','null','jkgjgjkg','jgjggkj','jgjgjk','20','kgjgjk','jkgjgjg','jjgjkgjk','kjgjgjk','2022-07-29','2030-12-31','jllhlhlj','klhkkklhklhl',302),(502,'BD_Estrategicas','BD_Impuestos','null','ihhhohoi','sqlserver','uggjhg','uguggg','InformacionPublicaClasificada','null','Alta','Alta','Alta','17','jhjhjkh','jkhjkhjh','khjhjkh','jbjhbjb','2010-12-17','2030-12-31','jhkjph','uphihiu',302),(503,'BD_Misionales','BD_Pasapotes',NULL,'hgggg','sqlserver','ggjgg','ghjghg','PublicaClasificada','null','uyyyu','yfyufyufuy','yfffuf','53','iuyuiuig','utytyutg','utytyut','yutytyu','2015-10-15','2030-10-31','uiyutu','uytuty',302),(504,'BD_Apoyo','BD_Educacion','SecretariaEducacion','kjhhjh','jhjh','kjhjhj','jhjih','InformacionPublica','null','yugyug','yugugu','ughghg','42','yuyyurytrf','ytryt','ytyt','ytrytrf','2015-10-15','2030-12-31','yfyuyuf','ytytytyugt',301),(506,'BD_EvaluacionyMejora','BD_Salud','prueba','uyuyuipy','ghghjghjg','gghjghg','gghgh','InformacionPublica','null','uggg','jigjgjig','jkghjgjkg','16','ygyuyg','gfhufghu','ffffuf','yfffufu','2020-12-10','2030-12-31','ghghg','gghghgh',302);
/*!40000 ALTER TABLE `bdyarchivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `computador`
--

DROP TABLE IF EXISTS `computador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `computador` (
  `id_computador` int NOT NULL,
  `tipo_computador` varchar(45) NOT NULL,
  `nom_computador` varchar(45) NOT NULL,
  `modelo_computador` varchar(45) DEFAULT NULL,
  `estado_computador` varchar(45) DEFAULT NULL,
  `serie_computador` varchar(45) DEFAULT NULL,
  `so_computador` varchar(45) DEFAULT NULL,
  `ubicacion_computador` varchar(30) DEFAULT NULL,
  `procesador_computador` varchar(45) DEFAULT NULL,
  `grupo_computador` varchar(45) DEFAULT NULL,
  `dominio_computador` varchar(45) DEFAULT NULL,
  `fabricante_computador` varchar(45) DEFAULT NULL,
  `fec_ing_activo_computador` date DEFAULT NULL,
  `fec_sal_activo_computador` date DEFAULT NULL,
  `memoria_computador` varchar(45) DEFAULT NULL,
  `direccion_ip_servidor` varchar(45) DEFAULT NULL,
  `marca_computador` varchar(45) DEFAULT NULL,
  `uuid_computador` varchar(45) DEFAULT NULL,
  `usuario_computador` varchar(45) DEFAULT NULL,
  `antivirus_computador` varchar(45) DEFAULT NULL,
  `tipo_disco_duro_computador` varchar(45) DEFAULT NULL,
  `dependencia_id_dependencia` varchar(10) DEFAULT NULL,
  `descripcion_computador` varchar(50) DEFAULT NULL,
  `clasificion_computador` varchar(50) DEFAULT NULL,
  `software_instalado_computador` varchar(50) DEFAULT NULL,
  `version_office_computador` varchar(50) DEFAULT NULL,
  `criticidad_computador` varchar(50) DEFAULT NULL,
  `responsable_computador` varchar(50) DEFAULT NULL,
  `accesos_computador` varchar(50) DEFAULT NULL,
  `contrato_mantenimiento_computador` varchar(50) DEFAULT NULL,
  `compatibilidadipv6_computador` varchar(50) DEFAULT NULL,
  `rol_computador` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_computador`),
  KEY `fk_computador_dependencia1_idx` (`dependencia_id_dependencia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `computador`
--

LOCK TABLES `computador` WRITE;
/*!40000 ALTER TABLE `computador` DISABLE KEYS */;
INSERT INTO `computador` VALUES (255,'PC_SecretaríasEstrategicas','hp255','a','a','a','w10','a','a','a','a','hp','2019-10-05','2030-10-10','a','a','hp','a','a','a','a','101','a','a','a','null','Alta','null','a','a','a','a'),(256,'PC_SecretaríasEstrategicas','dell256','b','b','b','w10','b','b','b','b','b','2015-05-11','2030-10-10','b','b','b','b','b','b','b','1.2','b','b','b','null','Alta','null','b','b','b','b');
/*!40000 ALTER TABLE `computador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dependencia`
--

DROP TABLE IF EXISTS `dependencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dependencia` (
  `id_dependencia` varchar(10) NOT NULL,
  `nom_dependencia` varchar(50) NOT NULL,
  `ubicacion_dependencia` varchar(50) DEFAULT NULL,
  `jefe_dependencia` varchar(50) DEFAULT NULL,
  `tel_dependencia` int DEFAULT NULL,
  `correo_dependencia` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id_dependencia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dependencia`
--

LOCK TABLES `dependencia` WRITE;
/*!40000 ALTER TABLE `dependencia` DISABLE KEYS */;
INSERT INTO `dependencia` VALUES ('1','DespachoGobernador','n','n',0,'n'),('1.1','SecretariaPrivadaGobernador','n','n',0,'n'),('1.1.1','DespachoSecretarioGobernador','n','n',0,'n'),('1.1.2','OficinaAsesoraProtocolo','n','n',0,'n'),('1.1.3','OficinaAsesoraComunicacionesyPrensa','n','n',0,'n'),('1.2','OficinaControlInterno','n','n',0,'n'),('1.3','OficinaControlDisciplinario','n','n',0,'n'),('1.4','OficinaGestionSocial','n','n',0,'n'),('1.5','OficinaGestiondelRiesgodeDesastre','n','n',0,'n'),('10','SecretariaSalud','n','n',0,'n'),('10.1','DespachoReconciliacionyMemoriaHistorica','n','n',0,'n'),('10.1.1','OficinaAsesoradePlaneacion','n','n',0,'n'),('10.1.2','OficinaAsesoraAsuntosJuridicos','n','n',0,'n'),('10.2','DireccionAseguramientoyPrestacionServicioSalud','n','n',0,'n'),('10.3','DireccionSaludPublica','n','n',0,'n'),('10.4','DireccionInspeccionVigilaciayControlSalud','n','n',0,'n'),('10.5','DireccionAdministrativayFinanciera','n','n',0,'n'),('11','SecretariaEducacion','n','n',0,'n'),('11.1','DespachoSecretarioEducacion','n','n',0,'n'),('11.1.2','OficinaAsesoraAsuntosJuridicosEducacion','n','n',0,'n'),('11.2','DireccionCoberturaEducativa','n','n',0,'n'),('11.3','DireccionCalidadEducativa','n','n',0,'n'),('11.4','DireccionInspeccionVigilaciayControlEducacion','n','n',0,'n'),('11.5','DireccionAdmPlantaEstablecimientosEducativos','n','n',0,'n'),('11.6','DireccionAdminitrativayFinancieraEducacion','n','n',0,'n'),('13','SecretariaInfraestructura','n','n',0,'n'),('13.1','DespachoSecretarioInfraestructura','n','n',0,'n'),('13.2','DireccionPlaneacionInfraestructura','n','n',0,'n'),('13.3','DireccionConstInterventoriaySupervisionObras','n','n',0,'n'),('14','SecretariaMovilidad','n','n',0,'n'),('14.1','DespachoSecretarioMovilidad','n','n',0,'n'),('14.2','DireccionPlaneacionMovilidadySeguridadVial','n','n',0,'n'),('14.3','DireccionSedesOperativas','n','n',0,'n'),('15','SecretariaAgriculturayDesarrolloRural','n','n',0,'n'),('15.1','DespachoSecretarioAgriculturayDesarrolloRural','n','n',0,'n'),('15.2','DireccionPlaneacionAgropecuariayDesarrolloRural','n','n',0,'n'),('15.3','DireccionDesAgroindustrialyAsistenciaTecnica','n','n',0,'n'),('16','SecretariaMinasyEnergia','n','n',0,'n'),('16.1','DespachoSecretarioMinasyEnergia','n','n',0,'n'),('2','ConsejosSuperioresContratacionPublica','n','n',0,'n'),('2.1','ConsejodeGobierno','n','n',0,'n'),('2.10','ConsejoDepartamentaldeEducacionSuperior','n','n',0,'n'),('2.11','ConsejoDepartamentaldePatrimonioCultural','n','n',0,'n'),('2.12','ConsejoDepartamentaldeArchivos','n','n',0,'n'),('2.13','ConsejoDepartamentaldeDesarrolloAdministrativo','n','n',0,'n'),('2.14','LosDemasConsejos','n','n',0,'n'),('2.2','ConsejoDepartamentaldeSeguridad','n','n',0,'n'),('2.3','ConsejoDepartamentalControlInterno','n','n',0,'n'),('2.4','ConsejoTerritorialdePlaneacion','n','n',0,'n'),('2.5','ConsejoDepartamentaldePoliticaEconomicaySocial','n','n',0,'n'),('2.6','ConsejoDepartamentaldePoliticaFiscal','n','n',0,'n'),('2.7','ConsejoDepartamentaldePoliticaSocial','n','n',0,'n'),('2.8','ConsejoDepartamentaldelRiesgodeDesastre','n','n',0,'n'),('2.9','ConsejoDepartamentaldeCienciayTecnologia','n','n',0,'n'),('3','SecretariaJuridica','n','n',0,'n'),('3.1','DespachoSecretarioJuridica','n','n',0,'n'),('3.2','DireccionContratacion','n','n',0,'n'),('3.3','DireccionDefensaJudicial','n','n',0,'n'),('3.4','DirConceptosActosAdministrativosyPersonasJuridicas','n','n',0,'n'),('4','SecretariaGeneral','n','n',0,'n'),('4.1','DespachoSecretarioGeneral','n','n',0,'n'),('4.2','DireccioFuncionPublica','n','n',0,'n'),('4.3','DirecciondeAtencionCiudadanoyGestionDocumental','n','n',0,'n'),('4.4','DireccionLogistica','n','n',0,'n'),('4.5','DireccionTics','n','n',0,'n'),('5','SecretariaPlaneacion','n','n',0,'n'),('5.1','DespachoSecretarioPlaneacion','n','n',0,'n'),('5.2','DireccionPlaneacionEstrategicaeInversionPublica','n','n',0,'n'),('5.3','DireccionEstudiosSocioecnomicoseInvestigacion','n','n',0,'n'),('6','SecretariaHacienda','n','n',0,'n'),('6.1','DespachoSecretarioHacienda','n','n',0,'n'),('6.2','OficinaCobroCoactivo','n','n',0,'n'),('6.3','DireccionPresupuesto','n','n',0,'n'),('6.4','DireccionContabilidad','n','n',0,'n'),('6.5','DireccionTesoreria','n','n',0,'n'),('6.6','DireccionIngresos','n','n',0,'n'),('6.7','DireccionEstudiosAnalisisFinancierosyFiscal','n','n',0,'n'),('6.8','DireccionFondoTerritorialPensiones','n','n',0,'n'),('7','SecretariaInterioryAsusntosGubernamentales','n','n',0,'n'),('7.1','DespachoSecretarioInterior','n','n',0,'n'),('7.2','OficinaJuventudes','n','n',0,'n'),('7.3','DireccionSeguridadyConvivenciaCiudadana','n','n',0,'n'),('7.4','DireccionAsistenciaMunicipal','n','n',0,'n'),('8','DireccionDesRegionalyOrdenamientoTerritorial','n','n',0,'n'),('8.1','DespachoSecretarioDesarrolloRegional','n','n',0,'n'),('8.2','DireccionAmbienteyDesarrolloSostenible','n','n',0,'n'),('8.3','DireccionGestionTerritorialMontesdeMaria','n','n',0,'n'),('8.4','DireccionGestionTerritorialSurBolivar','n','n',0,'n'),('8.5','DireccionGestionTerritorialNorteyDique','n','n',0,'n'),('9','SecretariaVictimayReconciliacion','n','n',0,'n'),('9.1','DespachoSecretarioVictimayReconciliacion','n','n',0,'n'),('9.2','DireccionReconciliacionyMemoriaHistorica','n','n',0,'n');
/*!40000 ALTER TABLE `dependencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipamientoauxiliar`
--

DROP TABLE IF EXISTS `equipamientoauxiliar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `equipamientoauxiliar` (
  `id_equipamiento_auxiliar` int NOT NULL,
  `tipo_equipamiento_auxiliar` varchar(45) NOT NULL,
  `nom_equipamiento_auxiliar` varchar(45) NOT NULL,
  `proceso_equipamiento_auxiliar` varchar(45) DEFAULT NULL,
  `descripcion_equipamiento_auxiliar` varchar(45) DEFAULT NULL,
  `ubicacion_equipamiento_auxiliar` varchar(45) DEFAULT NULL,
  `clasificacion_equipamiento_auxiliar` varchar(45) DEFAULT NULL,
  `criticidad_equipamiento_auxiliar` varchar(45) DEFAULT NULL,
  `responsable_equipamiento_auxiliar` varchar(45) DEFAULT NULL,
  `encargado_servidor` varchar(45) DEFAULT NULL,
  `accesos_equipamiento_auxiliar` varchar(45) DEFAULT NULL,
  `contrato_de_mantenimiento_equipamiento_auxiliar` varchar(45) DEFAULT NULL,
  `componente_equipamiento_auxiliar` varchar(45) DEFAULT NULL,
  `fec_ing_activo_equipamiento_auxiliar` date DEFAULT NULL,
  `fec_sal_activo_equipamiento_auxiliar` date DEFAULT NULL,
  `compatibilidadipv6_equipamiento_auxiliar` varchar(45) DEFAULT NULL,
  `direccion_ip_servidor` varchar(45) DEFAULT NULL,
  `marca_equipamiento_auxiliar` varchar(45) DEFAULT NULL,
  `modelo_equipamiento_auxiliar` varchar(45) DEFAULT NULL,
  `so_equipamiento_auxiliar` varchar(45) DEFAULT NULL,
  `software_instalado` varchar(45) DEFAULT NULL,
  `rol_equipamiento_auxiliar` varchar(45) DEFAULT NULL,
  `puertos_soportados` varchar(200) DEFAULT NULL,
  `Instalacionesfisicas_id_instalaciones_fisicas` int NOT NULL,
  PRIMARY KEY (`id_equipamiento_auxiliar`),
  KEY `fk_Equipamiento Auxiliar_Instalaciones Fisicas1_idx` (`Instalacionesfisicas_id_instalaciones_fisicas`),
  CONSTRAINT `fk_Equipamientoauxiliar_Instalacionesfisicas1` FOREIGN KEY (`Instalacionesfisicas_id_instalaciones_fisicas`) REFERENCES `instalacionesfisicas` (`id_instalaciones_fisicas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipamientoauxiliar`
--

LOCK TABLES `equipamientoauxiliar` WRITE;
/*!40000 ALTER TABLE `equipamientoauxiliar` DISABLE KEYS */;
INSERT INTO `equipamientoauxiliar` VALUES (102,'UPS','ups102','t','t','t','t','Alta','t','t','t','t','t','2019-10-20','2030-12-31','t','t','t','t','t','t','t','t',6);
/*!40000 ALTER TABLE `equipamientoauxiliar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `impacto`
--

DROP TABLE IF EXISTS `impacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `impacto` (
  `id_impacto` int NOT NULL AUTO_INCREMENT,
  `nom_impacto` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_impacto`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `impacto`
--

LOCK TABLES `impacto` WRITE;
/*!40000 ALTER TABLE `impacto` DISABLE KEYS */;
INSERT INTO `impacto` VALUES (1,'INSIGNIFICANTE'),(2,'MENOR'),(3,'MODERADO'),(4,'MAYOR'),(5,'CATASTROFICO');
/*!40000 ALTER TABLE `impacto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instalacionesfisicas`
--

DROP TABLE IF EXISTS `instalacionesfisicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `instalacionesfisicas` (
  `id_instalaciones_fisicas` int NOT NULL,
  `tipo_instalaciones_fisicas` varchar(45) NOT NULL,
  `nom_instalaciones_fisicas` varchar(45) NOT NULL,
  `proceso_instalaciones_fisicas` varchar(45) DEFAULT NULL,
  `descripcion_instalaciones_fisicas` varchar(45) DEFAULT NULL,
  `ubicacion_instalaciones_fisicas` varchar(45) DEFAULT NULL,
  `clasificacion_instalaciones_fisicas` varchar(45) DEFAULT NULL,
  `criticidad_instalaciones_fisicas` varchar(45) DEFAULT NULL,
  `responsable_instalaciones_fisicas` varchar(45) DEFAULT NULL,
  `encargado_instalaciones_fisicas` varchar(45) DEFAULT NULL,
  `accesos_instalaciones_fisicas` varchar(45) DEFAULT NULL,
  `contrato_de_mantenimiento_instalaciones_fisicas` varchar(45) DEFAULT NULL,
  `componente_instalaciones_fisicas` varchar(45) DEFAULT NULL,
  `fec_ing_activo_instalaciones_fisicas` date DEFAULT NULL,
  `fec_sal_activo_instalaciones_fisicas` date DEFAULT NULL,
  `compatibilidadipv6_instalaciones_fisicas` varchar(45) DEFAULT NULL,
  `direccion_ip_archivo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_instalaciones_fisicas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instalacionesfisicas`
--

LOCK TABLES `instalacionesfisicas` WRITE;
/*!40000 ALTER TABLE `instalacionesfisicas` DISABLE KEYS */;
INSERT INTO `instalacionesfisicas` VALUES (1,'DataCenter','DataCenterTorre1','a','a','a','a','Alta','a','a','a','a','a','2020-11-11','2030-12-30','a','a'),(2,'DataCenter','DataCenterTorre2','a','a','a','a','Alta','a','a','a','a','a','2020-11-11','2030-12-30','a','a'),(3,'edificio','t3','ygyguy','iuuigiug','giiugugg','gigiugugi','giuig','101','ffufuff','uygyufuygf','yuyuyuf','hhhugv','2020-12-14','2030-12-03','yugyguyg','yfyfyu'),(4,'edificio','t4','khhiuh','uihuuigi','iuggiugi','uiguigiupg','uiiugig','200','ghughu','yugyugu','yugugu','uggu','2020-12-15','2030-12-31','ggugiuip','ghgg'),(5,'edificio','t5','jggigig','ugiuggiugiu','uguigiugug','uigguigui','guiguig','205','hjghghugu','ugguigg','iuggigig','iuggig','2020-12-15','2030-12-03','ugghg','yugtyugyug'),(6,'edificio','t6','yufyofyfoyfy','yfyofyofoyfoy','yufyfyoufoyf','yufyuyufou','yufyuufouf','150','yuoghjhug','ughjgjhgjg','uyfyufuyf','jghjgjhghg','2020-12-15','2030-12-31','hghghg','jhghgh'),(7,'edificio','t7','jghjk','gggigigi','iuggigigi','hghgg','uiggig','403','hhhghgj','hjghghjg','hjghjghgj','jghjgjhgj','2020-12-14','2030-12-31','uihjhjh','jjjkj');
/*!40000 ALTER TABLE `instalacionesfisicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ips`
--

DROP TABLE IF EXISTS `ips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ips` (
  `id_ip` int NOT NULL AUTO_INCREMENT,
  `sede_ip` varchar(45) NOT NULL,
  `servicio_ip` varchar(45) DEFAULT NULL,
  `direccion_ip_mapeada` varchar(30) DEFAULT NULL,
  `direccion_ip_real` varchar(30) DEFAULT NULL,
  `puertos_ip` varchar(100) DEFAULT NULL,
  `descripcion_ip` varchar(100) DEFAULT NULL,
  `Redesycomunicaciones_id_redes_comunicaciones` int DEFAULT NULL,
  `Servidor_id_servidor` int DEFAULT NULL,
  PRIMARY KEY (`id_ip`),
  KEY `fk_Ips_Redes Comunicaciones1_idx` (`Redesycomunicaciones_id_redes_comunicaciones`),
  KEY `fk_Ips_Servidor1_idx` (`Servidor_id_servidor`),
  CONSTRAINT `fk_Ips_Redes Comunicaciones1` FOREIGN KEY (`Redesycomunicaciones_id_redes_comunicaciones`) REFERENCES `redesycomunicaciones` (`id_redes_comunicaciones`),
  CONSTRAINT `fk_Ips_Servidor1` FOREIGN KEY (`Servidor_id_servidor`) REFERENCES `servidor` (`id_servidor`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ips`
--

LOCK TABLES `ips` WRITE;
/*!40000 ALTER TABLE `ips` DISABLE KEYS */;
INSERT INTO `ips` VALUES (1,'Torre1','hghjghghjgjg','hjghjghjg','hghjghgjh','ghjghjgj','ghghjg',2000,302),(2,'Torre2','a','a','a','a','a',2000,302);
/*!40000 ALTER TABLE `ips` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matrizriesgo`
--

DROP TABLE IF EXISTS `matrizriesgo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `matrizriesgo` (
  `id_matrizriesgo` int NOT NULL AUTO_INCREMENT,
  `tipo_activo` varchar(45) DEFAULT NULL,
  `nom_activo` varchar(45) DEFAULT NULL,
  `num` varchar(45) DEFAULT NULL,
  `propiedad` varchar(45) DEFAULT NULL,
  `descripcion_riesgo` varchar(300) DEFAULT NULL,
  `responsable_riesgo` varchar(45) DEFAULT NULL,
  `amenaza_riesgo` varchar(100) DEFAULT NULL,
  `vulnerabilidad_riesgo` varchar(100) DEFAULT NULL,
  `probabilidad_inherente` varchar(45) DEFAULT NULL,
  `impacto_inherente` varchar(45) DEFAULT NULL,
  `zona_inherente` varchar(45) DEFAULT NULL,
  `opcion_manejo` varchar(45) DEFAULT NULL,
  `descripcion_control` varchar(300) DEFAULT NULL,
  `responsable_control` varchar(45) DEFAULT NULL,
  `probabilidad_residual` varchar(45) DEFAULT NULL,
  `impacto_residual` varchar(45) DEFAULT NULL,
  `zona_residual` varchar(45) DEFAULT NULL,
  `opcion_manejo_residual` varchar(100) DEFAULT NULL,
  `control_manejo` varchar(100) DEFAULT NULL,
  `actividad_manejo` varchar(100) DEFAULT NULL,
  `objetivo_manejo` varchar(100) DEFAULT NULL,
  `responsable_manejo` varchar(100) DEFAULT NULL,
  `periodo_manejo` varchar(100) DEFAULT NULL,
  `indicador_manejo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_matrizriesgo`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matrizriesgo`
--

LOCK TABLES `matrizriesgo` WRITE;
/*!40000 ALTER TABLE `matrizriesgo` DISABLE KEYS */;
INSERT INTO `matrizriesgo` VALUES (50,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(51,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(52,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(53,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(54,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(55,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(56,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(57,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(58,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(59,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(60,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(61,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(62,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(63,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(64,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(65,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(66,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(67,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(68,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(69,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(70,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(71,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(72,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(73,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(74,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(75,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(76,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(77,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(78,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(79,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(80,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(81,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(82,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(83,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(84,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(85,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(86,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(87,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(88,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(89,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(90,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(91,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(92,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(93,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(94,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(95,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(96,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(97,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(98,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(99,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(100,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(101,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(102,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(103,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(104,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(105,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(106,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(107,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(108,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(109,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(110,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(111,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(112,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(113,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(114,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(115,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(116,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(117,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(118,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(119,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(120,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(121,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(122,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(123,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(124,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(125,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(126,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(127,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(128,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(129,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(130,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(131,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(132,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(133,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(134,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','',''),(135,'Informacion','oracle','','Perdidadeconfidencialidad','','DespachoGobernador','Hurto_de_información','Desconocimiento o no aplicación de las políticas de seguridad y privacidad de la información','1','1','','ReducirelRiesgo','','DespachoGobernador','1','1','','','','','','','','');
/*!40000 ALTER TABLE `matrizriesgo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `microsoft`
--

DROP TABLE IF EXISTS `microsoft`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `microsoft` (
  `id_microsoft` varchar(50) NOT NULL,
  `grupo` varchar(45) NOT NULL,
  `familia` varchar(45) DEFAULT NULL,
  `version` float DEFAULT NULL,
  `cantidad_real` int DEFAULT NULL,
  `cantidad_asignada` int DEFAULT NULL,
  `Computador_id_computador` int DEFAULT NULL,
  PRIMARY KEY (`id_microsoft`),
  KEY `fk_Microsoft_Computador1_idx` (`Computador_id_computador`),
  CONSTRAINT `fk_Microsoft_Computador1` FOREIGN KEY (`Computador_id_computador`) REFERENCES `computador` (`id_computador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `microsoft`
--

LOCK TABLES `microsoft` WRITE;
/*!40000 ALTER TABLE `microsoft` DISABLE KEYS */;
INSERT INTO `microsoft` VALUES ('1010','Aplication','OfficeProfesional',10,101,101,255),('1011','System','Windows',10.1,200,150,256);
/*!40000 ALTER TABLE `microsoft` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persona` (
  `id_persona` int NOT NULL,
  `nom_persona` varchar(50) NOT NULL,
  `tipo_persona` varchar(45) DEFAULT NULL,
  `rol_persona` varchar(45) DEFAULT NULL,
  `proceso_persona` varchar(50) DEFAULT NULL,
  `clasificacion_persona` varchar(45) DEFAULT NULL,
  `justificacion_persona` varchar(100) DEFAULT NULL,
  `propietario_informacion_a_cargo` varchar(45) DEFAULT NULL,
  `accesos_persona` varchar(45) DEFAULT NULL,
  `fec_ing_activo_persona` date DEFAULT NULL,
  `fe_sal_activo_persona` date DEFAULT NULL,
  `Dependencia_id_dependencia` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_persona`),
  KEY `fk_Persona_Dependencia_idx` (`Dependencia_id_dependencia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (22222222,'Ana','Secretarías_EvaluacionyMejora','n','n','n','n','n','n','2018-01-10','2030-12-31','1.2'),(1047526452,'Rosa_Blanco','Secretarías_Apoyo','null','null','null','null','null','medio','2018-01-15','2022-12-31','1.1'),(1143524678,'Juan','Secretarías_Estrategicas','ytyy','utt8t','u7ttt','87tt8t','upttutut','uttut','2018-01-15','2030-12-31','104');
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `probabilidad`
--

DROP TABLE IF EXISTS `probabilidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `probabilidad` (
  `id_probabilidad` int NOT NULL AUTO_INCREMENT,
  `nom_probabilidad` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_probabilidad`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `probabilidad`
--

LOCK TABLES `probabilidad` WRITE;
/*!40000 ALTER TABLE `probabilidad` DISABLE KEYS */;
INSERT INTO `probabilidad` VALUES (1,'RARO'),(2,'IMPROBABLE'),(3,'POSIBLE'),(4,'PROBABLE'),(5,'CASI_SEGURO');
/*!40000 ALTER TABLE `probabilidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `redesycomunicaciones`
--

DROP TABLE IF EXISTS `redesycomunicaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `redesycomunicaciones` (
  `id_redes_comunicaciones` int NOT NULL,
  `tipo_redes_comunicaciones` varchar(45) NOT NULL,
  `nom_redes_comunicaciones` varchar(45) NOT NULL,
  `proceso_redes_comunicaciones` varchar(45) DEFAULT NULL,
  `descripcion_redes_comunicaciones` varchar(45) DEFAULT NULL,
  `ubicacion_redes_comunicaciones` varchar(45) DEFAULT NULL,
  `clasificacion_redes_comunicaciones` varchar(45) DEFAULT NULL,
  `criticidad_redes_comunicaciones` varchar(45) DEFAULT NULL,
  `responsable_redes_comunicaciones` varchar(45) DEFAULT NULL,
  `propietario_redes_comunicaciones` varchar(45) DEFAULT NULL,
  `accesos_soporte_informatico` varchar(45) DEFAULT NULL,
  `contrato_de_mantenimiento_redes_comunicaciones` varchar(45) DEFAULT NULL,
  `fec_ing_activo_redes_comunicaciones` date DEFAULT NULL,
  `fec_sal_activo_redes_comunicaciones` date DEFAULT NULL,
  `compatibilidadipv6_soporte_informatico` varchar(45) DEFAULT NULL,
  `direccion_ip_redes_comunicaciones` varchar(45) DEFAULT NULL,
  `marca_redes_comunicaciones` varchar(45) DEFAULT NULL,
  `modelo_redes_comunicaciones` varchar(45) DEFAULT NULL,
  `so_soporte_informatico` varchar(45) DEFAULT NULL,
  `software_instalado_redes_comunicaciones` varchar(45) DEFAULT NULL,
  `rol_redes_comunicaciones` varchar(45) DEFAULT NULL,
  `serial_redes_comunicaciones` varchar(45) DEFAULT NULL,
  `puertos_soportados_redes_comunicaciones` varchar(100) DEFAULT NULL,
  `instalacion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_redes_comunicaciones`),
  KEY `fk_Redes Comunicaciones_Instalaciones Fisicas1_idx` (`instalacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `redesycomunicaciones`
--

LOCK TABLES `redesycomunicaciones` WRITE;
/*!40000 ALTER TABLE `redesycomunicaciones` DISABLE KEYS */;
INSERT INTO `redesycomunicaciones` VALUES (2000,'SwitchesCore','Switch2000','hjkhjkhj','kjhjkhjb','ffgfguof','uyffyuf','Alta','50','yutyytuo','uytytyutyut','tyutytyu','2022-07-29','2030-10-31','iuggigh','d','iuguigiug','iuguipgipg','iuyuipuiph','uuiguigiu','iuyuigi','iuyuiuiyiu','iuyuiiugh','1'),(2001,'SwitchesCore','Switch2001','d','d','d','d','Alta','d','d','d','d','2020-12-15','2030-12-31','d','d','d','d','d','d','d','d','d','t1');
/*!40000 ALTER TABLE `redesycomunicaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `serviciosauxiliares`
--

DROP TABLE IF EXISTS `serviciosauxiliares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `serviciosauxiliares` (
  `id_servicio` int NOT NULL,
  `nom_servicio` varchar(45) NOT NULL,
  `tipo_servicio` varchar(45) NOT NULL,
  `proceso_servicio` varchar(45) DEFAULT NULL,
  `descripcion_servicio` varchar(100) DEFAULT NULL,
  `clasificacion_servicio` varchar(45) DEFAULT NULL,
  `criticidad_servicio` varchar(45) DEFAULT NULL,
  `responsable_servicio` varchar(45) DEFAULT NULL,
  `propietario_servicio` varchar(45) DEFAULT NULL,
  `accesos_servicio` varchar(45) DEFAULT NULL,
  `componentes_servicio` varchar(45) DEFAULT NULL,
  `contrato_de_mantenimiento_servicio` varchar(45) DEFAULT NULL,
  `fec_ing_servicio` date DEFAULT NULL,
  `fec_sal_servicio` date DEFAULT NULL,
  `compatibilidadipv6` varchar(45) DEFAULT NULL,
  `direccionip_servicio` varchar(15) DEFAULT NULL,
  `InstalacionesFisicas_id_instalacion` int DEFAULT NULL,
  PRIMARY KEY (`id_servicio`),
  KEY `fk_servicios auxiliares_instalaciones fisicas1_idx` (`InstalacionesFisicas_id_instalacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `serviciosauxiliares`
--

LOCK TABLES `serviciosauxiliares` WRITE;
/*!40000 ALTER TABLE `serviciosauxiliares` DISABLE KEYS */;
INSERT INTO `serviciosauxiliares` VALUES (1,'EnergiaT2','Energia','jkhjhjkh','jhjhjkhkj','jhjhjkh','Alta','a','jhjjkh','hjhjhhk','jjkhjhjkh','jhjkhjh','2020-12-15','2030-12-31','ugjjgjg','kjhjkhjkhjkh',2),(3,'f','AD','f','f','f','Alta','f','f','f','f','f','2020-12-15','2030-12-31','f','f',4);
/*!40000 ALTER TABLE `serviciosauxiliares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servidor`
--

DROP TABLE IF EXISTS `servidor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `servidor` (
  `id_servidor` int NOT NULL,
  `tipo_servidor` varchar(45) NOT NULL,
  `nom_servidor` varchar(45) NOT NULL,
  `proceso_servidor` varchar(45) DEFAULT NULL,
  `descripcion_servidor` varchar(45) DEFAULT NULL,
  `ubicacion_servidor` varchar(45) DEFAULT NULL,
  `clasificacion_servidor` varchar(45) DEFAULT NULL,
  `criticidad_servidor` varchar(45) DEFAULT NULL,
  `responsable_servidor` varchar(45) DEFAULT NULL,
  `encargado_servidor` varchar(45) DEFAULT NULL,
  `accesos_servidor` varchar(45) DEFAULT NULL,
  `contrato_de_mantenimiento_servidor` varchar(45) DEFAULT NULL,
  `componente_servidor` varchar(45) DEFAULT NULL,
  `fec_ing_activo_servidor` date DEFAULT NULL,
  `fec_sal_activo_servidor` date DEFAULT NULL,
  `compatibilidadipv6_archivo` varchar(45) DEFAULT NULL,
  `direccion_ip_servidor` varchar(45) DEFAULT NULL,
  `marca_servidor` varchar(45) DEFAULT NULL,
  `modelo_servidor` varchar(45) DEFAULT NULL,
  `so_servidor` varchar(45) DEFAULT NULL,
  `software_instalado` varchar(45) DEFAULT NULL,
  `rol_servidor` varchar(45) DEFAULT NULL,
  `serie_servidor` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_servidor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servidor`
--

LOCK TABLES `servidor` WRITE;
/*!40000 ALTER TABLE `servidor` DISABLE KEYS */;
INSERT INTO `servidor` VALUES (301,'Linux','Linux','jhjhh','kjjhjkhj','jkhjhjkh','jhjhjkh','jhjkhj','55','ghgjhgjhg','kjggghjg','ughghg','gghghjg','2015-05-30','2030-12-31','jhjhjhj','khjhjkñh','kjhjhjk','kjhjhkj','linux','hjghghjghj','jghgh','gghjghj'),(302,'rack','oracle','ishchsi','siuhcuishuhc','hxvhh','hshvshd','hvdjhvj','9542785','JuanBatista','hhhjih','guigig','uighuu','2019-10-20','2030-12-31','fftyfty','yfyfyufuo','uyfyfyufu','yufyfyufu','uygfyufyuf','yugfyufuf','yugyfyu','yugfyufuo'),(303,'MisionCritica','Linux303','x','x','x','x','Alta','x','x','x','x','x','2015-10-15','2030-12-31','x','x','x','x','x','x','x','x');
/*!40000 ALTER TABLE `servidor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soportesinformaticos`
--

DROP TABLE IF EXISTS `soportesinformaticos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `soportesinformaticos` (
  `id_soporte_informatico` int NOT NULL,
  `tipo_soporte_informatico` varchar(45) NOT NULL,
  `nom_soporte_informatico` varchar(45) NOT NULL,
  `proceso_soporte_informatico` varchar(45) DEFAULT NULL,
  `descripcion_soporte_informatico` varchar(45) DEFAULT NULL,
  `ubicacion_soporte_informatico` varchar(45) DEFAULT NULL,
  `clasificacion_servidor` varchar(45) DEFAULT NULL,
  `criticidad_soporte_informatico` varchar(45) DEFAULT NULL,
  `responsable_soporte_informatico` varchar(45) DEFAULT NULL,
  `propietario_soporte_informatico` varchar(45) DEFAULT NULL,
  `accesos_soporte_informatico` varchar(45) DEFAULT NULL,
  `contrato_de_mantenimiento_servidor` varchar(45) DEFAULT NULL,
  `fec_ing_activo_soporte_informatico` date DEFAULT NULL,
  `fec_sal_activo_soporte_informatico` date DEFAULT NULL,
  `compatibilidadipv6_soporte_informatico` varchar(45) DEFAULT NULL,
  `direccion_ip_servidor` varchar(45) DEFAULT NULL,
  `marca_soporte_informatico` varchar(45) DEFAULT NULL,
  `modelo_soporte_informatico` varchar(45) DEFAULT NULL,
  `so_soporte_informatico` varchar(45) DEFAULT NULL,
  `software_instalado_soporte_informatico` varchar(45) DEFAULT NULL,
  `rol_soporte_informatico` varchar(45) DEFAULT NULL,
  `serial_soporte_informatico` varchar(45) DEFAULT NULL,
  `puertos_soportados` varchar(100) DEFAULT NULL,
  `Dependencia_id_dependencia` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_soporte_informatico`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soportesinformaticos`
--

LOCK TABLES `soportesinformaticos` WRITE;
/*!40000 ALTER TABLE `soportesinformaticos` DISABLE KEYS */;
INSERT INTO `soportesinformaticos` VALUES (1,'Impresora','hp230-1','n','n','n','n','Alta','n','n','n','n','2020-12-15','2030-12-31','n','n','n','n','n','n','n','n','n','1.1.3');
/*!40000 ALTER TABLE `soportesinformaticos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `id_usu` int NOT NULL,
  `nom_usu` varchar(50) NOT NULL,
  `correo_usu` varchar(50) NOT NULL,
  `usuario` varchar(15) NOT NULL,
  `pass` varchar(15) NOT NULL,
  PRIMARY KEY (`id_usu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1143388,'ErlisVelasquez','erlis01@hotmail.com','erlis01','erlis123'),(9294632,'ElioFlorez','elioflorez06@hotmail.com','eflorez06','shelyanne17');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vulnerabilidad`
--

DROP TABLE IF EXISTS `vulnerabilidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vulnerabilidad` (
  `id_vulnerabilidad` int NOT NULL AUTO_INCREMENT,
  `nom_vulnerabilidad` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_vulnerabilidad`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vulnerabilidad`
--

LOCK TABLES `vulnerabilidad` WRITE;
/*!40000 ALTER TABLE `vulnerabilidad` DISABLE KEYS */;
INSERT INTO `vulnerabilidad` VALUES (1,'Desconocimiento_no_aplicación_de_las_políticas_de_seguridad_y_privacidad_de_la_información'),(2,'Manejo_manual_de_la_información'),(3,'Ausencia_de_validación_de_autenticación_de_la_información'),(4,'Ausencia_de_copias_de_respaldo_o_backups_de_la_información'),(5,'Retraso_en_la_salida_de_información_de_los_sistemas'),(6,'Retraso_en_la_salida_de_información_de_los_sistemas'),(7,'Retraso_en_la_entrega_de_información_por_parte_del_personal'),(8,'Información_sensible_sin_cifrado'),(9,'Ausencia_o_deficiencia_en_los_sistemas_de_autenticación_de_los_aplicativo'),(10,'Deficiencia_en_la_autorización_de_permisos_de_la_información'),(11,'Mantenimiento_insuficiente/instalación_fallida_de_los_medios_de_almacenamiento'),(12,'Ausencia_de_esquemas_de_reemplazo_periódico'),(13,'Susceptibilidad_a_la_humedad,el_polvo_y_la_suciedad'),(14,'Sensibilidad_a_la_radiación_electromagnética'),(15,'Ausencia_de_un_eficiente_control_de_cambios_en_la_configuración'),(16,'Susceptibilidad_a_la_variaciones_de_voltaje'),(17,'Susceptibilidad_a_las_variaciones_de_temperatura'),(18,'Almacenamiento_sin_protección'),(19,'Falta_de_cuidado_en_la_disposición_final'),(20,'Copia_no_controlada'),(21,'Ausencia_de_pruebas_de_envío_o_recepción_de_mensajes'),(22,'Líneas_de_comunicación_sin_protección'),(23,'Tráfico_sensible_sin_protección'),(24,'Conexión_deficiente_de_los_cables'),(25,'Punto_único_de_falla'),(26,'Ausencia_de_identificación_y_autenticación_de_emisor_y_receptor'),(27,'Arquitectura_insegura_de_la_red'),(28,'Transferencia_de_contraseñas_en_claro'),(29,'Gestión_inadecuada_de_la_red(Tolerancia_a_fallas_en_el_enrutamiento)'),(30,'Conexiones_de_red_pública_sin_protección'),(31,'Ausencia_o_insuficiencia_de_pruebas_de_software'),(32,'Defectos_bien_conocidos_en_el_software'),(33,'Ausencia_de_terminación_de_la_sesión_cuando_se_abandona_la_estación_de_trabajo'),(34,'Disposición_o_reutilización_de_los_medios_de_almacenamiento_sin_borrado_adecuado'),(35,'Ausencia_de_pistas_de_auditoría'),(36,'Asignación_errada_de_los_derechos_de_acceso'),(37,'Software_ampliamente_distribuido'),(38,'En_términos_de_tiempo_utilización_de_datos_errados_en_los_programas_de_aplicación'),(39,'Interfaz_de_usuario_compleja'),(40,'Ausencia_de_documentación'),(41,'Configuración_incorrecta_de_parámetros '),(42,'FechasIncorrectas'),(43,'Ausencia_de_mecanismo_de_identificación_y_autenticación,como_la_autenticación_de_usuario'),(44,'Tablas_de_contraseñas_sin_protección'),(45,'Gestión_deficiente_de_las_contraseñas'),(46,'Habilitación_de_servicios_innecesarios'),(47,'Software_nuevo_o_inmaduro'),(48,'Especificaciones_incompletas_o_no_claras_para_los_desarrolladores'),(49,'Ausencia_de_control_de_cambios_eficaz'),(50,'Descarga_y_uso_no_controlados_de_software'),(51,'Ausencia_de_copias_de_respaldo '),(52,'Ausencia_de_protección_física_de_la_edificación,puertas_y_ventanas'),(53,'Falla_en_la_producción_de_informes_de_gestión'),(54,'Ausencia_del_personal'),(55,'Procedimientos_inadecuados_de_contratación'),(56,'Entrenamiento_insuficiente_en_seguridad'),(57,'Uso_incorrecto_de_software_y_hardware'),(58,'Falla_de_conciencia_acerca_de_la_seguridad'),(59,'Ausencia_de_mecanismos_de_monitoreo'),(60,'Trabajo_o_supervisado_del_personal_externo_o_de_limpieza'),(61,'Ausencia_de_políticas_para_el_uso_correcto_de_los_medios_de_telecomunicaciones_y_mensajería '),(62,'Uso_inadecuado_o_descuidado_del_control_de_acceso_físico_a_las_edificaciones_y_los_recintos'),(63,'Ubicación_en_un_área_susceptible_de_inundación'),(64,'Red_energética_Inestable'),(65,'Ausencia_de procedimientos_para_la_presentación_de_informes_sobre_las_debilidades_en_la_seguridad'),(66,'Ausencia_de_revisiones_regulares_por_parte_de_la_gerencia'),(67,'Ausencia_de_mecanismos_de_monitoreo_establecidos_para_las_brechas_en_la_seguridad'),(68,'Ausencia_de_autorización_de_los_recursos_de_procesamiento_de_la_información '),(69,'Ausencia_o_insuficiencia_de_política_sobre_limpieza_de_escritorio_y_de_pantalla'),(70,'Ausencia_de_control_de_los_activos_que_se_encuentra_fuera_de_las_instalaciones'),(71,'Ausencia_de_procedimiento_formal_para_el_registro_y_retiro_de_usuarios '),(72,'Ausencia_de_proceso_formal_para_la_revisión(supervisión)de_los_derechos_de_acceso'),(73,'Ausencia_o_insuficiencia_de_disposiciones(con_respecto_a_la_seguridad)en_los_contratos_con_los_clientes_y/o_terceras_partes'),(74,'Ausencia_de_procedimiento_demonitoreo_de_los_recursos_de_procesamiento_de_información'),(75,'Ausencia_de_auditorías(supervisiones)regulares'),(76,'Ausencia_de_procedimientos_de_identificación_y_valoración_de_riesgos'),(77,'Ausencia_de_reportes_de_fallas_en_los_registros_de_administradores_y_operadores'),(78,'Respuesta_inadecuada_de_mantenimiento_de_servicio'),(79,'Ausencia_de_acuerdos_de_nivel_de_servicio,o_insuficiencia_en_los_mismos'),(80,'Ausencia_de_procedimiento_de_control_de_cambios'),(81,'Ausencia_de_procedimiento_formal_para_el_control_de_la_documentación_del_SGSI'),(82,'Ausencia_de_procedimiento_formal_para_la_supervisión_del_registro_del_SGSI'),(83,'Ausencia_de_procedimiento_formal_para_la_autorización_de_la_información_disponible_al_público'),(84,'Ausencia_de_asignación_adecuada_de_responsabilidades_en_la_seguridad_de_la_información'),(85,'Ausencia_de_planes_de_continuidad '),(86,'Ausencia_de_políticas_sobre_el_uso_del_correo_electrónico'),(87,'Ausencia_de_procedimientos_para_la_introducción_del_software_en_los_sistemas_operativos'),(88,'Ausencia_de_registros_en_las_bitácoras(logs)de_administrador_operario'),(89,'Ausencia_de_procedimientos_para_el_manejo_de_información_clasificada'),(90,'Ausencia_de_responsabilidades_en_la_seguridad_de_la_información_en_la_descripción_de_los_cargos'),(91,'Ausencia_de_procesos_disciplinarios_definidos_en_el_caso_de_incidentes_de_seguridad_de_la_información'),(92,'Ausencia_de_política_formal_sobre_la_utilización_de_computadores_portátiles'),(93,'Ausencia_de_procedimientos_de_identificación_y_valoración_de_riesgos');
/*!40000 ALTER TABLE `vulnerabilidad` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-04 11:28:06
